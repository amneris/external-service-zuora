package com.abaenglish.external.soap.objectmother;

import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.zuora.api.axis2.ZuoraServiceStub;

public class AccountObjectMother {

    public static ZuoraServiceStub.Account getAccountActive(){

        ZuoraServiceStub.Account account = new ZuoraServiceStub.Account();

        final ZuoraServiceStub.ID localId = new ZuoraServiceStub.ID();
        final ZuoraServiceStub.ID localSoldToId = new ZuoraServiceStub.ID();
        final ZuoraServiceStub.ID localBillToId = new ZuoraServiceStub.ID();

        localId.setID("2c92c0f9586bd97601586dce9d977af0");
        localSoldToId.setID("2c92c0f9586bd97601586dce9f2b7af1");
        localBillToId.setID("2c92c0f9586bd97601586dce9f2b7af1");

        account.setStatus(ZuoraExternalServiceSoap.AccountZStatus.ACTIVE.getValue());
        account.setSoldToId(localSoldToId);
        account.setBillToId(localBillToId);
        account.setId(localId);

        return account;
    }
    public static ZuoraServiceStub.Account getAccountCanceled(){

        ZuoraServiceStub.Account account = new ZuoraServiceStub.Account();

        final ZuoraServiceStub.ID localId = new ZuoraServiceStub.ID();
        final ZuoraServiceStub.ID localSoldToId = new ZuoraServiceStub.ID();
        final ZuoraServiceStub.ID localBillToId = new ZuoraServiceStub.ID();

        localId.setID("2c92c0f9586bd97601586dce9d977af0");
        localSoldToId.setID("2c92c0f9586bd97601586dce9f2b7af1");
        localBillToId.setID("2c92c0f9586bd97601586dce9f2b7af1");

        account.setStatus(ZuoraExternalServiceSoap.AccountZStatus.CANCELED.getValue());
        account.setSoldToId(localSoldToId);
        account.setBillToId(localBillToId);
        account.setId(localId);

        return account;
    }
}