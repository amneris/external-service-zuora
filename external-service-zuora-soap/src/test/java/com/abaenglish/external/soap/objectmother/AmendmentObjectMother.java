package com.abaenglish.external.soap.objectmother;

import com.zuora.api.axis2.ZuoraServiceStub;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AmendmentObjectMother {

    public static List<ZuoraServiceStub.Amendment> getAmendments() {

        List<ZuoraServiceStub.Amendment> amendments = new ArrayList<>();

        ZuoraServiceStub.Amendment amendment = new ZuoraServiceStub.Amendment();

        ZuoraServiceStub.ID id = new ZuoraServiceStub.ID();
        id.setID("2c92c0f8591b67de01591cb4f1763ed1");

        ZuoraServiceStub.ID SubscriptionId = new ZuoraServiceStub.ID();
        SubscriptionId.setID("2c92c09458fbeb32015906a6df5f7301");

        amendment.setId(id);
        amendment.setSubscriptionId(SubscriptionId);
        amendment.setUpdatedDate(Calendar.getInstance());
        amendment.setCode("CODEAMEND1");
        amendment.setStatus("Completed");
        amendments.add(amendment);

        ZuoraServiceStub.Amendment amendment2 = new ZuoraServiceStub.Amendment();

        ZuoraServiceStub.ID id2 = new ZuoraServiceStub.ID();
        id2.setID("2c92c0f8592005050159215dfd6e616a");

        ZuoraServiceStub.ID SubscriptionId2 = new ZuoraServiceStub.ID();
        SubscriptionId2.setID("2c92c0f9592011e601592101402d686d");

        amendment2.setId(id2);
        amendment2.setSubscriptionId(SubscriptionId2);
        amendment2.setUpdatedDate(Calendar.getInstance());
        amendment2.setCode("CODEAMEND2");
        amendment2.setStatus("Draft");
        amendments.add(amendment2);

        return amendments;
    }
}
