package com.abaenglish.external.soap.objectmother;

import com.zuora.api.axis2.ZuoraServiceStub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RefundObjectMother {
    public static List<ZuoraServiceStub.Refund> getRefunds() {
        List<ZuoraServiceStub.Refund> refunds = new ArrayList<>();

        for (int cnt = 1; cnt <= 5; cnt++) {
            ZuoraServiceStub.ID id = new ZuoraServiceStub.ID();
            id.setID(Integer.toString(cnt));
            ZuoraServiceStub.ID idAccount = new ZuoraServiceStub.ID();
            idAccount.setID(Integer.toString(cnt * 10));

            ZuoraServiceStub.Refund refund = new ZuoraServiceStub.Refund();

            refund.setId(id);
            refund.setAccountId(idAccount);
            refund.setAmount(new BigDecimal(cnt * 1000));
            refund.setRefundDate(new Date());
            refund.setUpdatedDate(Calendar.getInstance());
            refunds.add(refund);
        }
        return refunds;
    }
}
