package com.abaenglish.external.soap.service;


import com.abaenglish.external.soap.objectmother.*;
import com.abaenglish.external.zuora.soap.domain.subscription.*;
import com.abaenglish.external.zuora.soap.properties.ZuoraSoapProperties;
import com.abaenglish.external.zuora.soap.service.dto.CancelSubscriptionRequest;
import com.abaenglish.external.zuora.soap.service.dto.subscription.ZuoraAmendmentResponse;
import com.abaenglish.external.zuora.soap.service.impl.SubscriptionStatus;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.zuora.api.axis2.UnexpectedErrorFault;
import com.zuora.api.axis2.ZuoraServiceStub;
import com.zuora.api.util.ZApi;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.zuora.api.axis2.ZuoraServiceStub.ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertNotNull;

public class ZuoraExternalServiceTest {

    @InjectMocks
    private ZuoraExternalServiceSoap zuoraExternalService;
    @Mock
    private ZuoraSoapProperties ZuoraSoapProperties;
    @Mock
    private ZApi zApi;

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String INITIAL_DATE ="2016-12-13T00:00:00.000+01:00";
    private static final String FINAL_DATE ="2016-12-13T23:59:00.000+01:00";
    private static final SimpleDateFormat ZONE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
    private static final SimpleDateFormat formatZuora = new SimpleDateFormat("yyyy-MM-dd");

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);

        ZuoraSoapProperties.Soap soap = new ZuoraSoapProperties.Soap();
        soap.setUsername("pepe");
        soap.setPassword("pepe");
        soap.setHost("pepe");

        Mockito.when(ZuoraSoapProperties.getSoap()).thenReturn(soap);
    }


    @Test
    public void getUserAccountInfoOK(){

        ZuoraServiceStub.Account account  = AccountObjectMother.getAccountActive();

        ZuoraServiceStub.ZObject[] objects = new ZuoraServiceStub.ZObject[1];
        objects[0] = account;

        ZuoraServiceStub.QueryResult response = new ZuoraServiceStub.QueryResult();
        response.setDone(true);
        response.setSize(1);
        response.setRecords(objects);

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zQuery(Matchers.any())).thenReturn(response);

        ZuoraServiceStub.Account userAccount = zuoraExternalService.getUserAccount("2c92c0f9586bd97601586dce9d977af0");

        assertNotNull(userAccount);
        assertThat(userAccount.getId().getID()).isEqualTo(account.getId().getID());
        assertThat(userAccount.getStatus()).isEqualTo(ZuoraExternalServiceSoap.AccountZStatus.ACTIVE.getValue());
        assertThat(userAccount.getBillToId().getID()).isEqualTo(account.getBillToId().getID());
        assertThat(userAccount.getSoldToId().getID()).isEqualTo(account.getSoldToId().getID());

    }

    @Test
    public void changeAccountStatusInfoOK(){

        ZuoraServiceStub.Account account  = AccountObjectMother.getAccountActive();

        ZuoraServiceStub.ZObject[] objectsActive = new ZuoraServiceStub.ZObject[1];
        objectsActive[0] = account;

        ZuoraServiceStub.QueryResult responseActive = new ZuoraServiceStub.QueryResult();
        responseActive.setDone(true);
        responseActive.setSize(1);
        responseActive.setRecords(objectsActive);

        ZuoraServiceStub.ZObject[] objectsCanceled = new ZuoraServiceStub.ZObject[1];
        objectsActive[0] = account;

        ZuoraServiceStub.SaveResult[] saveResult = new ZuoraServiceStub.SaveResult[1];
        ZuoraServiceStub.SaveResult responseOK = new ZuoraServiceStub.SaveResult();
        responseOK.setSuccess(true);
        saveResult[0]=responseOK;

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zQuery(Matchers.any())).thenReturn(responseActive);
        Mockito.when(zApi.zUpdate(Matchers.any())).thenReturn(saveResult);

        final Set<String> selectClause = Stream.of("Id", "BillToId", "SoldToId", "Status").collect(Collectors.toSet());
        final Map<String, String> whereClause = new HashMap<String, String>() {{
            put("id", "2c92c0f9586bd97601586dce9d977af0");
        }};

        Boolean success = zuoraExternalService.changeAccountStatus("2c92c0f9586bd97601586dce9d977af0", ZuoraExternalServiceSoap.AccountZStatus.CANCELED);

        assertThat(success).isTrue();

    }

    @Test
    public void getUserAccountInfoKAO(){

        ZuoraServiceStub.QueryResult response = new ZuoraServiceStub.QueryResult();
        response.setDone(true);
        response.setSize(0);

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zQuery(Matchers.any())).thenReturn(response);

        ZuoraServiceStub.Account userAccount = zuoraExternalService.getUserAccount("2c92c0f9586bd97601586dce9d977af0");

        assertThat(userAccount).isNull();
    }

    @Test
    public void getRefundInfo() {

        List<ZuoraServiceStub.Refund> refunds = RefundObjectMother.getRefunds();

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zAdvancedQuery(Matchers.any())).thenReturn(refunds);

        List<ZuoraRefundResponse> zuoraRefundResponses = zuoraExternalService.getRefundInfo(INITIAL_DATE, null);
        assertNotNull(zuoraRefundResponses);
        assertThat(zuoraRefundResponses.get(0).getId()).isEqualTo(refunds.get(0).getId().getID());
        assertThat(zuoraRefundResponses.get(1).getAccountId()).isEqualTo(refunds.get(1).getAccountId().getID());
        assertThat(zuoraRefundResponses.get(2).getAmount()).isEqualTo(refunds.get(2).getAmount());
        assertThat(zuoraRefundResponses.get(3).getRefundDate()).isEqualTo(formatZuora.format(refunds.get(3).getRefundDate()));

        zuoraRefundResponses = zuoraExternalService.getRefundInfo(INITIAL_DATE, FINAL_DATE);
        assertNotNull(zuoraRefundResponses);
        assertThat(zuoraRefundResponses.get(0).getId()).isEqualTo(refunds.get(0).getId().getID());
        assertThat(zuoraRefundResponses.get(1).getAccountId()).isEqualTo(refunds.get(1).getAccountId().getID());
        assertThat(zuoraRefundResponses.get(2).getAmount()).isEqualTo(refunds.get(2).getAmount());
        assertThat(zuoraRefundResponses.get(3).getRefundDate()).isEqualTo(formatZuora.format(refunds.get(3).getRefundDate()));

    }
    @Test
    public void getSubscriptionsById() {

        List<String> subscriptions = new ArrayList<>();
        subscriptions.add("123456");
        subscriptions.add("654321");

        List<ZuoraServiceStub.Subscription> result = SubscriptionObjectMother.getSubscriptionInfo();

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zAdvancedQuery(Matchers.contains("FROM Subscription"))).thenReturn(result);

        List<ZuoraSubscriptionResponse> subscriptionInfos = zuoraExternalService.getSubscriptionsInfoById(subscriptions, Optional.empty());

        assertNotNull(subscriptionInfos);
        assertThat(subscriptionInfos.get(0).getId()).isEqualTo(result.get(0).getId().getID());
        assertThat(subscriptionInfos.get(1).getAccountId()).isEqualTo(result.get(1).getAccountId().getID());
        assertThat(subscriptionInfos.get(2).getTermEndDate()).isEqualTo(result.get(2).getTermEndDate());
        assertThat(subscriptionInfos.get(3).getTermStartDate()).isEqualTo(result.get(3).getTermStartDate());
        assertThat(subscriptionInfos.get(4).getCancelledDate()).isEqualTo(formatZuora.format(result.get(4).getCancelledDate()));

    }

    @Test
    public void getSubscriptionsByIdWithCancelledDateNull() {

        List<String> subscriptions = new ArrayList<>();
        subscriptions.add("123456");
        subscriptions.add("654321");

        List<ZuoraServiceStub.Subscription> result = SubscriptionObjectMother.getSubscriptionInfoCancelledDateNull();

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zAdvancedQuery(Matchers.contains("FROM Subscription"))).thenReturn(result);

        List<ZuoraSubscriptionResponse> subscriptionInfos = zuoraExternalService.getSubscriptionsInfoById(subscriptions, Optional.empty());

        assertNotNull(subscriptionInfos);
        assertThat(subscriptionInfos.get(0).getId()).isEqualTo(result.get(0).getId().getID());
        assertThat(subscriptionInfos.get(1).getAccountId()).isEqualTo(result.get(1).getAccountId().getID());
        assertThat(subscriptionInfos.get(2).getTermEndDate()).isEqualTo(result.get(2).getTermEndDate());
        assertThat(subscriptionInfos.get(3).getTermStartDate()).isEqualTo(result.get(3).getTermStartDate());
        assertThat(subscriptionInfos.get(4).getCancelledDate()).isNull();

    }

    @Test
    public void getSubscriptionsInfoByOriginalId() {

        List<String> subscriptions = new ArrayList<>();
        subscriptions.add("123456");
        subscriptions.add("654321");

        List<ZuoraServiceStub.Subscription> result = SubscriptionObjectMother.getSubscriptionInfo();

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zAdvancedQuery(Matchers.contains("FROM Subscription"))).thenReturn(result);

        List<ZuoraSubscriptionResponse> subscriptionsInfo = zuoraExternalService.getSubscriptionsInfoByOriginalId(subscriptions, Optional.of(SubscriptionStatus.ACTIVE));

        assertNotNull(subscriptionsInfo);
        assertThat(subscriptionsInfo.get(0).getId()).isEqualTo(result.get(0).getId().getID());
        assertThat(subscriptionsInfo.get(1).getAccountId()).isEqualTo(result.get(1).getAccountId().getID());
        assertThat(subscriptionsInfo.get(2).getTermEndDate()).isEqualTo(result.get(2).getTermEndDate());
        assertThat(subscriptionsInfo.get(3).getTermStartDate()).isEqualTo(result.get(3).getTermStartDate());
        assertThat(subscriptionsInfo.get(4).getCancelledDate()).isEqualTo(formatZuora.format(result.get(4).getCancelledDate()));

    }

    @Test
    public void getSubscriptionsInfoByOriginalIdCancelledDateNull() {

        List<String> subscriptions = new ArrayList<>();
        subscriptions.add("123456");
        subscriptions.add("654321");

        List<ZuoraServiceStub.Subscription> result = SubscriptionObjectMother.getSubscriptionInfoCancelledDateNull();

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zAdvancedQuery(Matchers.contains("FROM Subscription"))).thenReturn(result);

        List<ZuoraSubscriptionResponse> subscriptionsInfo = zuoraExternalService.getSubscriptionsInfoByOriginalId(subscriptions, Optional.of(SubscriptionStatus.ACTIVE));

        assertNotNull(subscriptionsInfo);
        assertThat(subscriptionsInfo.get(0).getId()).isEqualTo(result.get(0).getId().getID());
        assertThat(subscriptionsInfo.get(1).getAccountId()).isEqualTo(result.get(1).getAccountId().getID());
        assertThat(subscriptionsInfo.get(2).getTermEndDate()).isEqualTo(result.get(2).getTermEndDate());
        assertThat(subscriptionsInfo.get(3).getTermStartDate()).isEqualTo(result.get(3).getTermStartDate());
        assertThat(subscriptionsInfo.get(4).getCancelledDate()).isNull();

    }


    @Test
    public void getAmendmentInfo() {

        List<ZuoraServiceStub.Amendment> resultAmendmentInfo = AmendmentObjectMother.getAmendments();

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zAdvancedQuery(Matchers.anyString())).thenReturn(resultAmendmentInfo);

        List<ZuoraAmendmentResponse> zuoraAmendmentResponses = zuoraExternalService.getAmendmentInfo(INITIAL_DATE, null, ZuoraExternalServiceSoap.ZAmendmentTypes.Cancellation);
        assertNotNull(zuoraAmendmentResponses);
        assertThat(zuoraAmendmentResponses.get(0).getId()).isEqualTo(resultAmendmentInfo.get(0).getId().getID());
        assertThat(zuoraAmendmentResponses.get(0).getSubscriptionId()).isEqualTo(resultAmendmentInfo.get(0).getSubscriptionId().getID());
        assertThat(zuoraAmendmentResponses.get(0).getUpdatedDate()).isEqualTo(ZONE_FORMAT.format(resultAmendmentInfo.get(0).getUpdatedDate().getTime()));

        assertThat(zuoraAmendmentResponses.get(1).getId()).isEqualTo(resultAmendmentInfo.get(1).getId().getID());
        assertThat(zuoraAmendmentResponses.get(1).getSubscriptionId()).isEqualTo(resultAmendmentInfo.get(1).getSubscriptionId().getID());
        assertThat(zuoraAmendmentResponses.get(1).getUpdatedDate()).isEqualTo(ZONE_FORMAT.format(resultAmendmentInfo.get(1).getUpdatedDate().getTime()));

        zuoraAmendmentResponses = zuoraExternalService.getAmendmentInfo(INITIAL_DATE, FINAL_DATE, ZuoraExternalServiceSoap.ZAmendmentTypes.Cancellation);
        assertNotNull(zuoraAmendmentResponses);
        assertThat(zuoraAmendmentResponses.get(0).getId()).isEqualTo(resultAmendmentInfo.get(0).getId().getID());
        assertThat(zuoraAmendmentResponses.get(0).getSubscriptionId()).isEqualTo(resultAmendmentInfo.get(0).getSubscriptionId().getID());
        assertThat(zuoraAmendmentResponses.get(0).getUpdatedDate()).isEqualTo(ZONE_FORMAT.format(resultAmendmentInfo.get(0).getUpdatedDate().getTime()));

        assertThat(zuoraAmendmentResponses.get(1).getId()).isEqualTo(resultAmendmentInfo.get(1).getId().getID());
        assertThat(zuoraAmendmentResponses.get(1).getSubscriptionId()).isEqualTo(resultAmendmentInfo.get(1).getSubscriptionId().getID());
        assertThat(zuoraAmendmentResponses.get(1).getUpdatedDate()).isEqualTo(ZONE_FORMAT.format(resultAmendmentInfo.get(1).getUpdatedDate().getTime()));


    }

    @Test
    public void getSubscriptionbyId() {

        String subscriptionId = "123456789";
        List<ZuoraServiceStub.Subscription> result = SubscriptionObjectMother.getSubscriptionInfo();

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zAdvancedQuery(Matchers.any())).thenReturn(SubscriptionObjectMother.getSubscriptionInfo());

        List<ZuoraSubscriptionResponse> subscriptionInfos = zuoraExternalService.getSubscriptionInfoById(subscriptionId, ZuoraExternalServiceSoap.ZStatus.ACTIVE);
        assertNotNull(subscriptionInfos);
        assertThat(subscriptionInfos.get(0).getId()).isEqualTo(result.get(0).getId().getID());
        assertThat(subscriptionInfos.get(1).getAccountId()).isEqualTo(result.get(1).getAccountId().getID());
        assertThat(subscriptionInfos.get(2).getTermEndDate()).isEqualTo(result.get(2).getTermEndDate());
        assertThat(subscriptionInfos.get(3).getTermStartDate()).isEqualTo(result.get(3).getTermStartDate());
        assertThat(subscriptionInfos.get(4).getCancelledDate()).isEqualTo(formatZuora.format(result.get(4).getCancelledDate()));
    }

    @Test
    public void getInvoiceAmount() {

        String invoiceId = "123456789";

        BigDecimal amount = new BigDecimal("19.99");

        ZuoraServiceStub.Invoice invoice = new ZuoraServiceStub.Invoice();
        invoice.setAmount(amount);

        ZuoraServiceStub.ZObject[] objects = new ZuoraServiceStub.ZObject[1];
        objects[0] = invoice;

        ZuoraServiceStub.QueryResult response = new ZuoraServiceStub.QueryResult();
        response.setDone(true);
        response.setSize(1);
        response.setRecords(objects);
        Mockito.when(zApi.zLogin()).thenReturn(null);
        Mockito.when(zApi.zQuery(Matchers.any())).thenReturn(response);

        BigDecimal invoiceAmount = zuoraExternalService.getInvoiceAmount(invoiceId);

        assertNotNull(invoiceAmount);
        assertThat(invoiceAmount).isEqualTo(amount);

    }

    @Test
    public void getInvoiceInfo() {

        String invoiceId = "2c92c0f857899e1701578a4e5cb21385";
        String invoiceNumber = "INV00002521";
        BigDecimal amount = new BigDecimal(2399.99);

        ID id = new ID();
        id.setID(invoiceId);

        ZuoraServiceStub.Invoice invoice = new ZuoraServiceStub.Invoice();
        invoice.setId(id);
        invoice.setAmount(amount);
        invoice.setInvoiceNumber(invoiceNumber);

        ZuoraServiceStub.ZObject[] objects = new ZuoraServiceStub.ZObject[1];
        objects[0] = invoice;

        ZuoraServiceStub.QueryResult response = new ZuoraServiceStub.QueryResult();
        response.setDone(true);
        response.setSize(1);
        response.setRecords(objects);

        Mockito.when(zApi.zLogin()).thenReturn(null);
        Mockito.when(zApi.zQuery(Matchers.any())).thenReturn(response);

        InvoiceInfo invoiceInfo = zuoraExternalService.getInvoiceInfo(invoiceId);

        assertNotNull(invoiceInfo);
        assertThat(invoiceInfo.getId().toString()).isEqualTo(invoiceId);
        assertThat(invoiceInfo.getAmount()).isEqualTo(amount);
        assertThat(invoiceInfo.getInvoiceNumber()).isEqualTo(invoiceNumber);
    }

    @Test
    public void createSubscriptionZuora() throws UnexpectedErrorFault, RemoteException {
        ZuoraServiceStub.SubscribeResult[] response = SubscriptionObjectMother.subscribeResults();

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zSubscribe(Matchers.any())).thenReturn(response);

        ZuoraServiceStub.SubscribeResult[] responseTest = zuoraExternalService.createSubscription(SubscriptionObjectMother.createSubscriptionRequest());

        assertThat(responseTest[0].getSuccess()).isEqualTo(response[0].getSuccess());
        assertThat(responseTest[0].getAccountId().getID()).isEqualTo(response[0].getAccountId().getID());
    }

    @Test
    public void createSubscriptionZuoraPaypal() throws UnexpectedErrorFault, RemoteException {
        ZuoraServiceStub.SubscribeResult[] response = SubscriptionObjectMother.subscribeResults();

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zSubscribe(Matchers.any())).thenReturn(response);

        ZuoraServiceStub.SubscribeResult[] responseTest = zuoraExternalService.createSubscription(SubscriptionObjectMother.createSubscriptionRequestPaypal());

        assertThat(responseTest[0].getSuccess()).isEqualTo(response[0].getSuccess());
        assertThat(responseTest[0].getAccountId().getID()).isEqualTo(response[0].getAccountId().getID());
    }

    @Test
    public void createSubscriptionZuoraexistUser() throws UnexpectedErrorFault, RemoteException {
        ZuoraServiceStub.SubscribeResult[] response = SubscriptionObjectMother.subscribeResults();

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zSubscribe(Matchers.any())).thenReturn(response);

        ZuoraServiceStub.SubscribeResult[] responseTest = zuoraExternalService.createSubscription(SubscriptionObjectMother.createSubscriptionRequestExistUser());

        assertThat(responseTest[0].getSuccess()).isEqualTo(response[0].getSuccess());
        assertThat(responseTest[0].getAccountId().getID()).isEqualTo(response[0].getAccountId().getID());
    }


    @Test
    public void testFormatDateTimezone() {

        String date = zuoraExternalService.formatDate(new Date());

        LocalDateTime localDate = LocalDateTime.now();
        DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_FORMAT);
        assertThat(format.format(localDate)).isEqualTo(date);
    }

    @Test
    public void cancelSubscription() throws UnexpectedErrorFault, RemoteException {
        final String subscriptionId = "2c92c0f9585841e601586888bed85cac";
        final ZuoraServiceStub.AmendResult amendResult = new ZuoraServiceStub.AmendResult();
        amendResult.setSuccess(true);

        final ID subscriptionZId = new ID();
        subscriptionZId.setID(subscriptionId);
        amendResult.setSubscriptionId(subscriptionZId);

        final ID amendZId = new ID();
        amendZId.setID("2c92c0f9585841e601586888984f5bef");
        amendResult.setAmendmentIds(new ID[]{ amendZId });

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        final ZuoraServiceStub.AmendResult[] mockedResponse = new ZuoraServiceStub.AmendResult[] {amendResult};
        Mockito.when(zApi.zAmend(Matchers.any(), Matchers.any())).thenReturn(mockedResponse);

        final CancelSubscriptionRequest request = CancelSubscriptionRequest.Builder.aCancelSubscriptionRequest()
                .subscriptionId(subscriptionId)
                .contractEffectiveDate(new Date())
                .effectiveDate(new Date())
                .name("Test cancel")
                .generateInvoice(false)
                .processPayments(false)
                .build();

        ZuoraServiceStub.AmendResult[] amendResponse = zuoraExternalService.cancelSubscription(request);
        assertThat(amendResponse[0].getSuccess()).isEqualTo(amendResult.getSuccess());
        assertThat(amendResponse[0].getSubscriptionId().getID()).isEqualTo(amendResult.getSubscriptionId().getID());
        assertThat(amendResponse[0].getAmendmentIds()[0].getID()).isEqualTo(amendResult.getAmendmentIds()[0].getID());
    }

    @Test
    public void cancelSubscriptionFails() throws UnexpectedErrorFault, RemoteException {
        final CancelSubscriptionRequest request = CancelSubscriptionRequest.Builder.aCancelSubscriptionRequest()
                .subscriptionId("test")
                .contractEffectiveDate(new Date())
                .effectiveDate(new Date())
                .name("Test cancel")
                .generateInvoice(false)
                .processPayments(false)
                .build();

        assertThatThrownBy(() -> zuoraExternalService.cancelSubscription(request))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Subscription identifier is not valid");
    }

    @Test
    public void getInvoicePayments() {

        ID zId = new ID();
        zId.setID("2c92c0f857899e1701578a4e5cb21385");

        ID zPaymentId = new ID();
        zPaymentId.setID("2c92c0f857899e1701578a4e5cb21385");

        ID zInvoiceId = new ID();
        zInvoiceId.setID("2c92c0f857899e1701578a4e5cb21385");

        ZuoraServiceStub.InvoicePayment zInvoicePayment = new ZuoraServiceStub.InvoicePayment();
        zInvoicePayment.setId(zId);
        zInvoicePayment.setPaymentId(zPaymentId);
        zInvoicePayment.setInvoiceId(zInvoiceId);

        ZuoraServiceStub.ZObject[] objects = new ZuoraServiceStub.ZObject[1];
        objects[0] = zInvoicePayment;

        ZuoraServiceStub.QueryResult response = new ZuoraServiceStub.QueryResult();
        response.setDone(true);
        response.setSize(1);
        response.setRecords(objects);

        Mockito.when(zApi.zLogin()).thenReturn(null);
        Mockito.when(zApi.zQuery(Matchers.any())).thenReturn(response);

        List<InvoicePayment> invoicePayment = zuoraExternalService.getInvoicePayment(zPaymentId.getID());

        assertNotNull(invoicePayment);
        assertThat(invoicePayment.get(0).getId().toString()).isEqualTo(zId.getID());
        assertThat(invoicePayment.get(0).getPaymentId().toString()).isEqualTo(zPaymentId.getID());
        assertThat(invoicePayment.get(0).getInvoiceId().toString()).isEqualTo(zInvoiceId.getID());
    }


    @Test
    public void getAmendmentBySubscriptionId() {

        final ZuoraServiceStub.Amendment amend = AmendmentObjectMother.getAmendments().get(0);
        final String subscriptionId = amend.getSubscriptionId().toString();

        ZuoraServiceStub.QueryResult response = new ZuoraServiceStub.QueryResult();
        response.setDone(true);
        ZuoraServiceStub.ZObject[] objects = Stream.of(amend).toArray( size -> new ZuoraServiceStub.ZObject[size] );
        response.setRecords(objects);
        response.setSize(objects.length);

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zQuery(Matchers.contains("FROM Amendment"))).thenReturn(response);

        List<ZuoraAmendmentResponse> zuoraAmendmentResponses = zuoraExternalService.getAmendmentByIdFilters(Optional.of(subscriptionId), Optional.empty(), Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal));
        assertNotNull(zuoraAmendmentResponses);
        assertThat(zuoraAmendmentResponses.get(0).getId()).isEqualTo(amend.getId().getID());
        assertThat(zuoraAmendmentResponses.get(0).getSubscriptionId()).isEqualTo(amend.getSubscriptionId().getID());
        assertThat(zuoraAmendmentResponses.get(0).getUpdatedDate()).isEqualTo(ZONE_FORMAT.format(amend.getUpdatedDate().getTime()));
        assertThat(zuoraAmendmentResponses.get(0).getCode()).isEqualTo(amend.getCode());
        assertThat(zuoraAmendmentResponses.get(0).getStatus()).isEqualTo(amend.getStatus());
    }

    @Test
    public void getPayment() {

        ZuoraServiceStub.QueryResult response = new ZuoraServiceStub.QueryResult();
        response.setDone(true);
        ZuoraServiceStub.ZObject[] objects = PaymentObjectMother.getPayments().stream().toArray( size -> new ZuoraServiceStub.ZObject[size] );
        response.setRecords(objects);
        response.setSize(objects.length);

        Mockito.when(zApi.zLogin(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        Mockito.when(zApi.zQuery(Matchers.contains("FROM Payment"))).thenReturn(response);

        Optional<Payment> zuoraResponses = zuoraExternalService.getPaymentProcessedById(PaymentObjectMother.getPayments().get(0).getId().toString());
        assertNotNull(zuoraResponses);
        assertThat(zuoraResponses.get().getId()).isEqualTo(PaymentObjectMother.getPayments().get(0).getId().getID());
        assertThat(zuoraResponses.get().getPaymentNumber()).isEqualTo(PaymentObjectMother.getPayments().get(0).getPaymentNumber());
        assertThat(zuoraResponses.get().getStatus()).isEqualTo(PaymentObjectMother.getPayments().get(0).getStatus());
        assertThat(zuoraResponses.get().getAccountId()).isEqualTo(PaymentObjectMother.getPayments().get(0).getAccountId().getID().toString());
        assertThat(zuoraResponses.get().getAmount()).isEqualTo(PaymentObjectMother.getPayments().get(0).getAmount());
        assertThat(zuoraResponses.get().getEffectiveDate()).isEqualTo(PaymentObjectMother.getPayments().get(0).getEffectiveDate());
        assertThat(zuoraResponses.get().getGateway()).isEqualTo(PaymentObjectMother.getPayments().get(0).getGateway());
        assertThat(zuoraResponses.get().getGatewayResponse()).isEqualTo(PaymentObjectMother.getPayments().get(0).getGatewayResponse());
    }

}