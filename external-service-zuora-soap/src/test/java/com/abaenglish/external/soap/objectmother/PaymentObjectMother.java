package com.abaenglish.external.soap.objectmother;

import com.zuora.api.axis2.ZuoraServiceStub;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class PaymentObjectMother {

    private static ZuoraServiceStub.ID buildZuoraId(final String id) {
        final ZuoraServiceStub.ID zuoraId = new ZuoraServiceStub.ID();
        zuoraId.setID(id);
        return zuoraId;
    }

    public static List<ZuoraServiceStub.Payment> getPayments() {
        final ZuoraServiceStub.Payment payment = new ZuoraServiceStub.Payment();
        payment.setId(buildZuoraId("2c92c09559b0489c0159cb955aed2fc8"));
        payment.setPaymentNumber("P-00005254");
        payment.setStatus("Processed");
        payment.setAccountId(buildZuoraId("2c92c0f9588ff00b0158923c53b2252b"));
        payment.setAmount(BigDecimal.valueOf(19.99));
        payment.setEffectiveDate("2017-01-23");
        payment.setGateway("Stripe Gateway");
        payment.setGatewayResponse("Payment complete.");
        return Arrays.asList(payment);
    }
}
