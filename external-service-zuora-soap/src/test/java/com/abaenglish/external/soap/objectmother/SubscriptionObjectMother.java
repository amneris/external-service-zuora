package com.abaenglish.external.soap.objectmother;

import com.abaenglish.external.zuora.soap.service.dto.CreateSubscriptionRequest;
import com.abaenglish.external.zuora.soap.service.dto.subscription.ZuoraAmendmentResponse;
import com.zuora.api.axis2.ZuoraServiceStub;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.zuora.api.axis2.ZuoraServiceStub.ID;
import static com.zuora.api.axis2.ZuoraServiceStub.SubscribeResult;

public class SubscriptionObjectMother {

    public static CreateSubscriptionRequest createSubscriptionRequest() {
        List<String> list = new ArrayList<>();
        list.add("123456456");

        CreateSubscriptionRequest request = CreateSubscriptionRequest.Builder.aCreateSubscriptionRequest()
                .userId(265L)
                .name("Name")
                .surname("Surname")
                .country("CAN")
                .currency("MXN")
                .email("student+265@abaenglish.com")
                .type("Credit Card")
                .paymentId("123456789")
                .ratePlanId(list)
                .period(12)
                .zuoraAccountId(null)
                .zuoraAccountNumber(null)
                .build();

        return request;
    }

    public static CreateSubscriptionRequest createSubscriptionRequestPaypal() {
        List<String> list = new ArrayList<>();
        list.add("123456456");

        CreateSubscriptionRequest request = CreateSubscriptionRequest.Builder.aCreateSubscriptionRequest()
                .userId(265L)
                .name("Name")
                .surname("Surname")
                .country("USA")
                .currency("MXN")
                .email("student+265@abaenglish.com")
                .type("PayPal")
                .paypalBaid("I-123456789")
                .paypalEmail("supervisorr-buyer@hotmail.com")
                .paymentId("123456789")
                .ratePlanId(list)
                .period(12)
                .zuoraAccountId(null)
                .zuoraAccountNumber(null)
                .build();

        return request;
    }

    public static CreateSubscriptionRequest createSubscriptionRequestExistUser() {
        List<String> list = new ArrayList<>();
        list.add("123456456");

        CreateSubscriptionRequest request = CreateSubscriptionRequest.Builder.aCreateSubscriptionRequest()
                .userId(265L)
                .name("Name")
                .surname("Surname")
                .country("MEX")
                .currency("MXN")
                .email("student+265@abaenglish.com")
                .type("Credit Card")
                .paymentId("123456789")
                .ratePlanId(list)
                .period(12)
                .zuoraAccountId("12345689")
                .zuoraAccountNumber("ACCOUNT123456789")
                .build();

        return request;
    }

    public static SubscribeResult[] subscribeResults() {
        ID accountId = new ID();
        accountId.setID("987654321");
        SubscribeResult[] results = new SubscribeResult[1];
        SubscribeResult result = new SubscribeResult();
        result.setSuccess(true);
        result.setAccountNumber("123456789");
        result.setAccountId(accountId);

        results[0] = result;
        return results;
    }

    public static List<ZuoraServiceStub.Subscription> getSubscriptionInfo() {

        List<ZuoraServiceStub.Subscription> list = new ArrayList<>();

        for (int i = 1; i < 6; i++) {
            ID id = new ID();
            id.setID(Integer.toString(i));

            ZuoraServiceStub.Subscription firstSubscription = new ZuoraServiceStub.Subscription();
            firstSubscription.setAccountId(id);
            firstSubscription.setId(id);
            firstSubscription.setOriginalId(id);
            firstSubscription.setTermEndDate(LocalDate.now().plusYears(1).toString());
            firstSubscription.setTermStartDate(LocalDate.now().toString());
            firstSubscription.setSubscriptionEndDate(LocalDate.now().plusYears(1).toString());
            firstSubscription.setSubscriptionStartDate(LocalDate.now().toString());
            firstSubscription.setUpdatedDate(Calendar.getInstance());
            firstSubscription.setCancelledDate(new Date());

            list.add(firstSubscription);
        }
        return list;
    }


    public static List<ZuoraServiceStub.Subscription> getSubscriptionInfoCancelledDateNull() {

        List<ZuoraServiceStub.Subscription> list = new ArrayList<>();

        for (int i = 1; i < 6; i++) {
            ID id = new ID();
            id.setID(Integer.toString(i));

            ZuoraServiceStub.Subscription firstSubscription = new ZuoraServiceStub.Subscription();
            firstSubscription.setAccountId(id);
            firstSubscription.setId(id);
            firstSubscription.setOriginalId(id);
            firstSubscription.setTermEndDate(LocalDate.now().plusYears(1).toString());
            firstSubscription.setTermStartDate(LocalDate.now().toString());
            firstSubscription.setUpdatedDate(Calendar.getInstance());
//            firstSubscription.setCancelledDate(null);

            list.add(firstSubscription);
        }
        return list;
    }


    public static  List<ZuoraServiceStub.Amendment> getAmendments(){

        List<ZuoraServiceStub.Amendment> list = new ArrayList<>();

        for (int i = 1; i < 2; i++) {
            ID id = new ID();
            id.setID(Integer.toString(i));
            ZuoraServiceStub.Amendment amendment = new ZuoraServiceStub.Amendment();
            amendment.setId(id);
            amendment.setSubscriptionId (id);
            amendment.setUpdatedDate(Calendar.getInstance());

            list.add(amendment);
        }
        return list;

    }


    public static List<ZuoraAmendmentResponse> getZuoraAmendmentResponse() {

        List<ZuoraAmendmentResponse> list = new ArrayList<>();

        list.add(ZuoraAmendmentResponse.Builder.aZuoraAmendmentResponse()
                .id("2c92c0f8591b67de01591cb4f1763ed1")
                .subscriptionId("2c92c09458fbeb32015906a6df5f7301")
                .build());

//        list.add(ZuoraAmendmentResponse.Builder.aZuoraAmendmentResponse()
//                .id("2c92c0f8592005050159215dfd6e616a")
//                .subscriptionId("2c92c0f9592011e601592101402d686d")
//                .UpdatedDate("2016-12-21T13:32:04.000+01:00")
//                .build());

        return list;
    }

}
