package com.abaenglish.external.zuora.soap.service.impl;

public enum SubscriptionStatus {
    ACTIVE("Active"),

    EXPIRED("Expired"),

    CANCELLED("Cancelled");

    private String value;

    private SubscriptionStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
