package com.abaenglish.external.zuora.soap.service.impl;

import com.abaenglish.external.zuora.soap.domain.subscription.*;
import com.abaenglish.external.zuora.soap.properties.ZuoraSoapProperties;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.dto.CancelSubscriptionRequest;
import com.abaenglish.external.zuora.soap.service.dto.CreateSubscriptionRequest;
import com.abaenglish.external.zuora.soap.service.dto.subscription.ZuoraAmendmentResponse;
import com.zuora.api.axis2.UnexpectedErrorFault;
import com.zuora.api.axis2.ZuoraServiceStub;
import com.zuora.api.util.ZApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ZuoraExternalServiceSoap implements IZuoraExternalServiceSoap {

    private static final String PATTERN = "yyyy-MM-dd";
    private static final Logger LOGGER = LoggerFactory.getLogger(ZuoraExternalServiceSoap.class);
    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
    private SimpleDateFormat formatZuoraSdf = new SimpleDateFormat(PATTERN);

    @Autowired
    private ZuoraSoapProperties zuoraSoapProperties;
    private ZApi zApi;

    @PostConstruct
    public void addInterceptor() {
        zApi = new ZApi("https://" + zuoraSoapProperties.getSoap().getHost() + "/apps/services/a/79.0");
    }

    @Override
    public Boolean changeAccountStatus(String accountId, AccountZStatus accountZStatus) {

        // validate required paramenters to avoid zuora errors
        Assert.notNull(accountId, "Account identifier is required");
        Assert.notNull(accountZStatus, "ZStatus is required");

        Boolean success = false;

        ZuoraServiceStub.Account userAccount;

        loginZuoraSoap();

        userAccount = getUserAccount(accountId);

        LOGGER.debug("Account obtained from id '{}'", accountId);

        if (userAccount != null) {
            if (userAccount.getStatus().equals(accountZStatus.getValue())) {
                success = true;
            } else {
                userAccount.setStatus(accountZStatus.getValue());
                // Update in Zuora
                ZuoraServiceStub.SaveResult[] updateResults = zApi.zUpdate(new ZuoraServiceStub.ZObject[]{userAccount});

                success = updateResults[0].getSuccess();

                LOGGER.debug("Updated account with id '{}' and with the result '{}'", accountId, success);

                if (!success) {
                    LOGGER.error("An errors occurs while we were updating the account with the following error: '{}'", updateResults[0].getErrors()[0].getMessage());
                }
            }
        }

        return success;
    }

    @Override
    public ZuoraServiceStub.Account getUserAccount(String accountId) {

        Assert.notNull(accountId, "Account identifier is required");

        final Set<String> selectClause = Stream.of("Id", "BillToId", "SoldToId", "Status").collect(Collectors.toSet());
        final Map<String, String> whereClause = new HashMap<String, String>() {{
            put("id", accountId);
        }};

        ZuoraServiceStub.QueryResult result = queryToZuora("Account", selectClause, whereClause);

        ZuoraServiceStub.Account userAccount;

        if (result.getDone() && result.getSize() == 1) {
            userAccount = (ZuoraServiceStub.Account) result.getRecords()[0];
        } else {
            userAccount = null;
            LOGGER.error("There is more than one or none account for the following accountId '{}'", accountId);
        }

        return userAccount;

    }

    @Override
    public ZuoraServiceStub.SubscribeResult[] createSubscription(CreateSubscriptionRequest request) throws UnexpectedErrorFault, RemoteException {

        ZuoraServiceStub.SubscribeRequest subscribeRequest = prepareSubscribeRequest(request);

        loginZuoraSoap();

        return zApi.zSubscribe(new ZuoraServiceStub.SubscribeRequest[]{subscribeRequest});
    }

    @Override
    public BigDecimal getInvoiceAmount(String invoiceId) {

        ZuoraServiceStub.Invoice invoice = new ZuoraServiceStub.Invoice();

        loginZuoraSoap();

        ZuoraServiceStub.QueryResult result = zApi.zQuery(String.format("SELECT Amount FROM Invoice WHERE id='%s'", invoiceId));

        if (result.getDone() && result.getSize() == 1) {

            invoice = (ZuoraServiceStub.Invoice) result.getRecords()[0];
        }

        return invoice.getAmount();
    }

    @Override
    public InvoiceInfo getInvoiceInfo(String invoiceId) {

        ZuoraServiceStub.Invoice invoice = new ZuoraServiceStub.Invoice();

        zApi.zLogin();

        ZuoraServiceStub.QueryResult result = zApi.zQuery(String.format("SELECT Id, InvoiceNumber, Amount, DueDate FROM Invoice WHERE id='%s'", invoiceId));

        if (result.getDone() && result.getSize() == 1) {

            invoice = (ZuoraServiceStub.Invoice) result.getRecords()[0];
        }

        return convertToInvoiceInfo(invoice);
    }

    @Override
    public List<InvoiceItemsInfo> getInvoiceItemsInfo(String invoiceId) {

        final Set<String> selectClause = Stream.of("Id", "ProductId", "ProductName", "AccountingCode", "AppliedToInvoiceItemId", "ChargeAmount", "ChargeName", "ChargeDescription", "ChargeType",
                "InvoiceId", "Quantity", "RatePlanChargeId", "TaxAmount", "TaxCode", "TaxExemptAmount", "TaxMode", "UnitPrice", "SKU", "SubscriptionId").collect(Collectors.toSet());
        final Map<String, String> whereClause = new HashMap<String, String>() {{
            put("InvoiceId", invoiceId);
        }};

        loginZuoraSoap();
        // search invoice items
        ZuoraServiceStub.QueryResult result = queryToZuora("InvoiceItem", selectClause, whereClause);

        if (result.getDone() && result.getSize() > 0) {
            return Stream.of(result.getRecords())
                    .map(ZuoraServiceStub.InvoiceItem.class::cast)
                    .map(item -> InvoiceItemsInfo.Builder.anInvoiceItemsInfo()
                            .id(item.getId() != null ? item.getId().getID() : null)
                            .productId(item.getProductId() != null ? item.getProductId().getID() : null)
                            .productName(item.getProductName())
                            .accountingCode(item.getAccountingCode())
                            .appliedToInvoiceItemId(item.getAppliedToInvoiceItemId() != null ? item.getAppliedToInvoiceItemId().getID() : null)
                            .chargeAmount(item.getChargeAmount())
                            .chargeName(item.getChargeName())
                            .chargeDescription(item.getChargeDescription())
                            .chargeType(item.getChargeType())
                            .invoiceId(item.getInvoiceId().getID())
                            .quantity(item.getQuantity() != null ? item.getQuantity().intValue() : null)
                            .ratePlanChargeId(item.getRatePlanChargeId() != null ? item.getRatePlanChargeId().getID() : null)
                            .taxAmount(item.getTaxAmount())
                            .taxCode(item.getTaxCode())
                            .taxExemptAmount(item.getTaxExemptAmount())
                            .taxMode(item.getTaxMode())
                            .unitPrice(item.getUnitPrice())
                            .SKU(item.getSKU())
                            .subscriptionId(item.getSubscriptionId() != null ? item.getSubscriptionId().getID() : null)
                            .build())
                    .collect(Collectors.toList());
        }
        return new ArrayList<>(0);
    }

    /**
     * @see {@link IZuoraExternalServiceSoap#getInvoicePayment(String)}
     */
    @Override
    public List<InvoicePayment> getInvoicePayment(final String paymentId) {

        final Set<String> selectClause = Stream.of("Id", "InvoiceId", "PaymentId").collect(Collectors.toSet());
        final Map<String, String> whereClause = new HashMap<String, String>() {{
            put("PaymentId", paymentId);
        }};

        loginZuoraSoap();
        // search invoice payments
        ZuoraServiceStub.QueryResult result = queryToZuora("InvoicePayment", selectClause, whereClause);

        if (result.getDone() && result.getSize() > 0) {
            return Stream.of(result.getRecords())
                    .map(ZuoraServiceStub.InvoicePayment.class::cast)
                    .map(item -> InvoicePayment.Builder.anInvoicePayment()
                            .id(item.getId() != null ? item.getId().getID() : null)
                            .invoiceId(item.getInvoiceId() != null ? item.getInvoiceId().getID() : null)
                            .paymentId(item.getPaymentId() != null ? item.getPaymentId().getID() : null)
                            .build())
                    .collect(Collectors.toList());
        }
        return new ArrayList<>(0);
    }

    @Override
    public Optional<Payment> getPaymentProcessedById(final String paymentId) {

        final Set<String> selectClause = Stream.of("Id", "PaymentNumber","Status", "AccountId", "Amount", "CancelledOn", "EffectiveDate", "Gateway", "GatewayResponse").collect(Collectors.toSet());
        final Map<String, String> whereClause = new HashMap<String, String>() {{
            put("Id", paymentId);
            put("Status", "Processed");
        }};

        loginZuoraSoap();
        // search payments
        final ZuoraServiceStub.QueryResult result = queryToZuora("Payment", selectClause, whereClause);

        if(result.getSize() > 1) {
            throw new IllegalStateException("Too many results found");
        }
        if (result.getDone() && result.getSize() > 0) {
            return Optional.of(result.getRecords()[0])
                    .map(ZuoraServiceStub.Payment.class::cast)
                    .map(item -> Payment.Builder.aPayment()
                            .id(item.getId() != null ? item.getId().getID() : null)
                            .paymentNumber(item.getPaymentNumber())
                            .status(item.getStatus())
                            .accountId(item.getAccountId() != null ? item.getAccountId().getID() : null)
                            .amount(item.getAmount())
                            .cancelledOn(item.getCancelledOn())
                            .effectiveDate(item.getEffectiveDate())
                            .gateway(item.getGateway())
                            .gatewayResponse(item.getGatewayResponse())
                            .build());
        }
        return Optional.empty();
    }

    @Override
    public List<ZuoraRefundResponse> getRefundInfo(String startingDay, String finishingDay) {

        Assert.notNull(startingDay, "StartingDay is required");

        loginZuoraSoap();

        Set<String> selectClause = Stream.of("Id", "AccountId", "Amount", "RefundDate", "UpdatedDate").collect(Collectors.toSet());
        Map<String, String> whereClause;

        if (finishingDay == null) {
            whereClause = new HashMap<String, String>() {{
                put("Status = ", "Processed");
                put("UpdatedDate > ", startingDay);
            }};
        } else {
            whereClause = new HashMap<String, String>() {{
                put("Status = ", "Processed");
                put("UpdatedDate >= ", startingDay);
                put("UpdatedDate <= ", finishingDay);
            }};
        }

        return convertToRefundInfo(queryToZuoraAllRecords("Refund", selectClause, whereClause));
    }

    @Override
    public ZuoraRefundResponse getRefundInfoById(final String refundId, Optional<RefundStatus> status) {

        Assert.notNull(refundId, "RefundId is required");

        loginZuoraSoap();

        Set<String> selectClause = Stream.of("Id", "AccountId", "Amount", "RefundDate", "UpdatedDate").collect(Collectors.toSet());
        Map<String, String> whereClause;

        if (status.isPresent()) {
            whereClause = new HashMap<String, String>() {{
                put("Status = ", status.get().getValue());
                put("Id = ", refundId);
            }};
        } else {
            whereClause = new HashMap<String, String>() {{
                put("Id = ", refundId);
            }};
        }

        List<ZuoraRefundResponse> zuoraRefundResponses = convertToRefundInfo(queryToZuoraAllRecords("Refund", selectClause, whereClause));

        ZuoraRefundResponse zuoraRefundResponse = null;

        if (!zuoraRefundResponses.isEmpty()) {
            zuoraRefundResponse = zuoraRefundResponses.get(0);
        }

        return zuoraRefundResponse;
    }

    /**
     * {@link IZuoraExternalServiceSoap#getAmendmentInfo}
     */

    @Override
    public List<ZuoraAmendmentResponse> getAmendmentInfo(String startingDay, String finishingDay, ZAmendmentTypes zAmendmentTypes) {
        // validate required paramenters to avoid zuora errors
        Assert.notNull(startingDay, "startingDay is required");
        Assert.notNull(zAmendmentTypes.name(), "Amendment type is required");

        loginZuoraSoap();

        Set<String> selectClause = Stream.of("Id", "subscriptionId", "UpdatedDate").collect(Collectors.toSet());
        Map<String, String> whereClause;

        if (finishingDay == null) {
            whereClause = new HashMap<String, String>() {{
                put("Status = ", "Completed");
                put("Type = ", zAmendmentTypes.name());
                put("UpdatedDate > ", startingDay);
            }};
        } else {
            whereClause = new HashMap<String, String>() {{
                put("Status = ", "Completed");
                put("Type = ", zAmendmentTypes.name());
                put("UpdatedDate > ", startingDay);
                put("UpdatedDate <= ", finishingDay);
            }};
        }

        List<ZuoraServiceStub.Amendment> amendments = queryToZuoraAllRecords("Amendment", selectClause, whereClause);

        List<ZuoraAmendmentResponse> zuoraAmendmentResponses = new ArrayList<>();

        for (ZuoraServiceStub.Amendment amendment : amendments) {
            zuoraAmendmentResponses.add(ZuoraAmendmentResponse.Builder.aZuoraAmendmentResponse()
                    .id(amendment.getId().toString())
                    .subscriptionId(amendment.getSubscriptionId().toString())
                    .updatedDate(formatter.format(amendment.getUpdatedDate().getTime()))
                    .build());
        }

        return zuoraAmendmentResponses;
    }

    @Override
    public List<ZuoraAmendmentResponse> getAmendmentByIdFilters(final Optional<String> subscriptionId, final Optional<String> amendmentId, final Optional<ZAmendmentTypes> zAmendmentType) {

        loginZuoraSoap();

        final Set<String> selectClause = Stream.of("Id", "Code", "SubscriptionId", "UpdatedDate", "Status", "Type").collect(Collectors.toSet());
        final Map<String, String> whereClause = new HashMap<String, String>();
        subscriptionId.ifPresent( i -> whereClause.put("SubscriptionId", i) );
        amendmentId.ifPresent( a -> whereClause.put("Id", a) );
        zAmendmentType.ifPresent( t -> whereClause.put("Type", t.name()) );

        final ZuoraServiceStub.QueryResult result = queryToZuora("Amendment", selectClause, whereClause);

        final List<ZuoraAmendmentResponse> zuoraAmendmentResponses = new ArrayList<>();

        if(result.getSize() > 0 && result.getDone() && result.getRecords().length > 0) {
            zuoraAmendmentResponses.addAll(Stream.of(result.getRecords())
                    .map(ZuoraServiceStub.Amendment.class::cast)
                    .map( a -> ZuoraAmendmentResponse.Builder.aZuoraAmendmentResponse()
                            .id(a.getId().toString())
                            .subscriptionId(a.getSubscriptionId().toString())
                            .updatedDate(formatter.format(a.getUpdatedDate().getTime()))
                            .code(a.getCode())
                            .status(a.getStatus())
                            .type(a.getType())
                            .build())
                    .collect(Collectors.toList()));
        }

        return zuoraAmendmentResponses;
    }

    @Override
    public List<ZuoraSubscriptionResponse> getSubscriptionsInfoById(final Collection<String> subscriptions, Optional<String> status) {

        loginZuoraSoap();

        List subscriptionResponses = new ArrayList<>();

        for (String subscription : subscriptions) {
            subscriptionResponses.addAll(convertToSubscriptionInfoPure(getSubscriptionInfoByUniqueId(subscription, status)));
        }

        return subscriptionResponses;
    }

    /**
     *{@link IZuoraExternalServiceSoap#getSubscriptionsInfoByOriginalId(Collection, Optional)}
     */
    @Override
    public List<ZuoraSubscriptionResponse> getSubscriptionsInfoByOriginalId(final Collection<String> subscriptions, final Optional<SubscriptionStatus> status) {

        loginZuoraSoap();

        final List subscriptionResponses = new ArrayList<>();

        final Set<String> selectClause = Stream.of("Id", "OriginalId", "AccountId", "TermEndDate", "TermStartDate", "UpdatedDate", "SubscriptionStartDate","CancelledDate", "PreviousSubscriptionId").collect(Collectors.toSet());
        final List<ZuoraServiceStub.Subscription> zuoraSubscriptionsResponse = new ArrayList<>();

        final Map<String, String> whereClause = new HashMap();
        status.ifPresent( s -> whereClause.put("Status = ", s.getValue()));

        for (String subscription : subscriptions) {
            whereClause.put("OriginalId = ", subscription);
            zuoraSubscriptionsResponse.addAll(queryToZuoraAllRecords("Subscription", selectClause, whereClause));
        }
        subscriptionResponses.addAll(convertToSubscriptionInfoPure(zuoraSubscriptionsResponse));

        return subscriptionResponses;
    }

    private List<ZuoraServiceStub.Subscription> getSubscriptionInfoByUniqueId(final String subscription, Optional<String> status) {

        Set<String> selectClause = Stream.of("Id", "OriginalId", "AccountId", "TermEndDate", "TermStartDate", "UpdatedDate", "SubscriptionStartDate","CancelledDate", "PreviousSubscriptionId", "SubscriptionStartDate", "SubscriptionEndDate").collect(Collectors.toSet());

        Map<String, String> whereClause = new HashMap<String, String>() {{
            put("Id = ", subscription);
        }};
        status.ifPresent( s -> whereClause.put("Status = ", s) );

        return queryToZuoraAllRecords("Subscription", selectClause, whereClause);

    }

    @Deprecated
    @Override
    public List<ZuoraSubscriptionResponse> getSubscriptionInfoById(String subscriptionId, ZStatus zStatus) {
//        TODO: Remember to make a refactor.
//        String query;
        loginZuoraSoap();

//        switch (zStatus) {
//            case ACTIVE:
//                query = String.format("SELECT Id, AccountId, TermStartDate, TermEndDate, SubscriptionStartDate, OriginalId FROM Subscription WHERE id='%s'"
//                        , subscriptionId);
//                break;
//            default: //never happens
//                query = "";
//                break;
//        }

//        List<ZuoraServiceStub.Subscription> result = zApi.zAdvancedQuery(query);
//        List<ZuoraSubscriptionResponse> subscriptionInfos = convertToSubscriptionInfo(result);
        return convertToSubscriptionInfo(getSubscriptionInfoByUniqueId(subscriptionId, Optional.empty()));
    }

    private List<ZuoraSubscriptionResponse> convertToSubscriptionInfo(List<ZuoraServiceStub.Subscription> result) {

        List<ZuoraSubscriptionResponse> subscriptionInfos = new ArrayList<>();

        if (!result.isEmpty()) {
            for (ZuoraServiceStub.Subscription subscription : result) {
                ZuoraSubscriptionResponse subscriptionInfo = new ZuoraSubscriptionResponse();
                if (subscription.getOriginalId() != null) {
                    subscriptionInfo.setId(subscription.getOriginalId().toString());
                } else {
                    subscriptionInfo.setId(subscription.getId().toString());
                }
                subscriptionInfo.setAccountId(subscription.getAccountId().toString());
                subscriptionInfo.setTermEndDate(subscription.getTermEndDate());
                subscriptionInfo.setTermStartDate(subscription.getTermStartDate());
                Optional.ofNullable(subscription.getCancelledDate())
                        .ifPresent( cancelledDate -> subscriptionInfo.setCancelledDate(formatZuoraSdf.format(cancelledDate)));
                subscriptionInfos.add(subscriptionInfo);
            }
        }
        return subscriptionInfos;
    }

    private List<ZuoraSubscriptionResponse> convertToSubscriptionInfoPure(List<ZuoraServiceStub.Subscription> result) {

        List<ZuoraSubscriptionResponse> subscriptionInfos = new ArrayList<>();

        if (!result.isEmpty()) {
            for (ZuoraServiceStub.Subscription subscription : result) {
                ZuoraSubscriptionResponse subscriptionInfo = new ZuoraSubscriptionResponse();
                subscriptionInfo.setId(subscription.getId().toString());
                subscriptionInfo.setOriginalId(subscription.getOriginalId().toString());
                subscriptionInfo.setAccountId(subscription.getAccountId().toString());
                subscriptionInfo.setTermEndDate(subscription.getTermEndDate());
                subscriptionInfo.setTermStartDate(subscription.getTermStartDate());
                subscriptionInfo.setSubscriptionStartDate(subscription.getSubscriptionStartDate());
                subscriptionInfo.setSubscriptionEndDate(subscription.getSubscriptionEndDate());
                Optional.ofNullable(subscription.getCancelledDate())
                        .ifPresent( cancelledDate -> subscriptionInfo.setCancelledDate(formatZuoraSdf.format(cancelledDate)));
                Optional.ofNullable(subscription.getPreviousSubscriptionId()).ifPresent( s -> subscriptionInfo.setPreviousSubscriptionId(s.toString()));
                subscriptionInfos.add(subscriptionInfo);
            }
        }
        return subscriptionInfos;
    }

    private List<ZuoraRefundResponse> convertToRefundInfo(List<ZuoraServiceStub.Refund> result) {

        List<ZuoraRefundResponse> zuoraRefundResponses = new ArrayList<>();

        if (!result.isEmpty()) {
            for (ZuoraServiceStub.Refund refund : result) {
                ZuoraRefundResponse zuoraRefundResponse = new ZuoraRefundResponse();
                zuoraRefundResponse.setId(refund.getId().toString());
                zuoraRefundResponse.setAccountId(refund.getAccountId().toString());
                zuoraRefundResponse.setAmount(refund.getAmount());
                zuoraRefundResponse.setRefundDate(formatZuoraSdf.format(refund.getRefundDate()));
                zuoraRefundResponse.setUpdatedDate(formatter.format(refund.getUpdatedDate().getTime()));
                zuoraRefundResponses.add(zuoraRefundResponse);
            }
        }
        return zuoraRefundResponses;
    }

    private InvoiceInfo convertToInvoiceInfo(ZuoraServiceStub.Invoice result) {

        InvoiceInfo invoiceInfo = new InvoiceInfo();

        if (result != null) {
            invoiceInfo.setId(result.getId().toString());
            invoiceInfo.setInvoiceNumber(result.getInvoiceNumber());
            invoiceInfo.setAmount(result.getAmount());
            invoiceInfo.setDueDate(result.getDueDate());
        }
        return invoiceInfo;
    }

    private ZuoraServiceStub.SubscribeRequest prepareSubscribeRequest(CreateSubscriptionRequest request) {

        ZuoraServiceStub.Account acc;

        //CREATE ACCOUNT
        if (request.getZuoraAccountId() == null) {
            acc = prepareAccount(request.getName(), request.getCurrency(), request.getUserId().toString(), request.getGatewayName());
        } else {
            acc = getAccountZuora(request.getZuoraAccountId());
        }

        //CREATE CONTACT
        ZuoraServiceStub.Contact contact;
        contact = prepareContact(request.getName(), request.getSurname(), request.getCountry(), request.getEmail());

        //CREATE PAYMENT METHOD
        ZuoraServiceStub.PaymentMethod paymentMethod;
        paymentMethod = preparePaymentMethod(request);

        //CREATE SUBSCRIPTION DATA
        ZuoraServiceStub.SubscriptionData subscriptionData;
        subscriptionData = prepareSubscriptionData(request.getPeriod(), request.getRatePlanId());

        ZuoraServiceStub.SubscribeRequest finalRequest = new ZuoraServiceStub.SubscribeRequest();
        finalRequest.setAccount(acc);
        finalRequest.setBillToContact(contact);
        finalRequest.setSoldToContact(contact);
        finalRequest.setPaymentMethod(paymentMethod);
        finalRequest.setSubscriptionData(subscriptionData);

        return finalRequest;

    }

    private ZuoraServiceStub.Account prepareAccount(String username, String currency, String userId, String gatewayName) {

        ZuoraServiceStub.Account acc = new ZuoraServiceStub.Account();
        acc.setBatch("Batch1");
        acc.setBcdSettingOption("AutoSet");
        acc.setBillCycleDay(0);
        acc.setName(username);
        acc.setAllowInvoiceEdit(true);
        acc.setAutoPay(true);
        acc.setPaymentTerm("Due Upon Receipt");
        acc.setCurrency(currency);
        acc.setAba_userId__c(userId);
        acc.setPaymentGateway(gatewayName);
        acc.setInvoiceDeliveryPrefsEmail(zuoraSoapProperties.isAccountInvoiceDeliveryPrefsEmail());

        return acc;
    }

    private ZuoraServiceStub.Account getAccountZuora(String zuoraAccountId) {
        ZuoraServiceStub.ID id = new ZuoraServiceStub.ID();
        id.setID(zuoraAccountId);

        ZuoraServiceStub.Account acc = new ZuoraServiceStub.Account();
        acc.setId(id);
        return acc;
    }

    private ZuoraServiceStub.Contact prepareContact(String username, String surname, String country, String email) {
        ZuoraServiceStub.Contact contact = new ZuoraServiceStub.Contact();
        contact.setFirstName(username);
        contact.setLastName(surname);
        contact.setCountry(country);

        switch (country) {
            case "USA":
                contact.setState("AE");
                break;
            case "CAN":
                contact.setState("NU");
                break;
            default:
                break;
        }

        contact.setWorkEmail(email);
        contact.setPersonalEmail(email);
        return contact;
    }

    private ZuoraServiceStub.PaymentMethod preparePaymentMethod(CreateSubscriptionRequest request) {
        ZuoraServiceStub.PaymentMethod paymentMethod = new ZuoraServiceStub.PaymentMethod();

        switch (request.getType()) {
            case "PayPal":
                paymentMethod.setPaypalBaid(request.getPaypalBaid());
                paymentMethod.setPaypalEmail(request.getPaypalEmail());
                paymentMethod.setType(request.getType());
                paymentMethod.setPaypalType("ExpressCheckout");
                break;
            default:
                ZuoraServiceStub.ID id = new ZuoraServiceStub.ID();
                id.setID(request.getPaymentId());
                paymentMethod.setId(id);
                break;
        }
        return paymentMethod;
    }

    private ZuoraServiceStub.SubscriptionData prepareSubscriptionData(Integer period, List<String> ratePlanList) {

        //CREATE SUBSCRIPTION
        String date = formatDate(new Date());
        ZuoraServiceStub.Subscription subscription = new ZuoraServiceStub.Subscription();
        subscription.setTermStartDate(date);
        subscription.setContractEffectiveDate(date);
        subscription.setServiceActivationDate(date);
        subscription.setIsInvoiceSeparate(true);
        subscription.setAutoRenew(true);
        subscription.setInitialTerm(period);
        subscription.setRenewalTerm(period);

        //RATEPLAN SEARCH
        ZuoraServiceStub.RatePlanData[] ratePlanData = new ZuoraServiceStub.RatePlanData[ratePlanList.size()];

        for (Integer i = 0; i < ratePlanList.size(); i++) {
            ZuoraServiceStub.RatePlanData oneRatePlanData = new ZuoraServiceStub.RatePlanData();
            ZuoraServiceStub.RatePlan ratePlan = new ZuoraServiceStub.RatePlan();

            ZuoraServiceStub.ID idRatePlan = new ZuoraServiceStub.ID();
            idRatePlan.setID(ratePlanList.get(i));
            ratePlan.setProductRatePlanId(idRatePlan);
            oneRatePlanData.setRatePlan(ratePlan);

            ratePlanData[i] = oneRatePlanData;
        }

        //CREATE SUBSCRIPTION DATA
        ZuoraServiceStub.SubscriptionData subscriptionData = new ZuoraServiceStub.SubscriptionData();
        subscriptionData.setRatePlanData(ratePlanData);
        subscriptionData.setSubscription(subscription);
        return subscriptionData;

    }

    /**
     * This method change the original date and convert date with timeZone Europe/Madrid
     * GMT+1
     */
    public String formatDate(Date dateTime) {
        LocalDateTime ldt = LocalDateTime.ofInstant(dateTime.toInstant(), ZoneId.of("Europe/Madrid"));
        DateTimeFormatter format = DateTimeFormatter.ofPattern(PATTERN);
        return format.format(ldt);
    }

    public void loginZuoraSoap() {
        zApi.zLogin(zuoraSoapProperties.getSoap().getUsername(), zuoraSoapProperties.getSoap().getPassword());
    }

    //    /**
//     * @see IZuoraExternalServiceSoap#cancelSubscription(CancelSubscriptionRequest)
//     */
    @Override
    public ZuoraServiceStub.AmendResult[] cancelSubscription(final CancelSubscriptionRequest cancelSubscriptionRequest) throws UnexpectedErrorFault, RemoteException {

        // validate required paramenters to avoid zuora errors
        Assert.notNull(cancelSubscriptionRequest.getSubscriptionId(), "Subscription identifier is required");
        Assert.isTrue(isValidZuoraIdentifier(cancelSubscriptionRequest.getSubscriptionId()), "Subscription identifier is not valid");
        Assert.notNull(cancelSubscriptionRequest.getContractEffectiveDate(), "Contract effective date is required");
        Assert.notNull(cancelSubscriptionRequest.getEffectiveDate(), "Effective date is required");
        Assert.notNull(cancelSubscriptionRequest.getName(), "Name is required");

        LOGGER.debug("Cancelling subscription with id '{}'", cancelSubscriptionRequest.getSubscriptionId());

        // create amend request
        final ZuoraServiceStub.ID id = new ZuoraServiceStub.ID();
        id.setID(cancelSubscriptionRequest.getSubscriptionId());

        final ZuoraServiceStub.Amendment amendment = new ZuoraServiceStub.Amendment();
        amendment.setType(ZAmendmentTypes.Cancellation.name());
        amendment.setName(cancelSubscriptionRequest.getName());
        amendment.setSubscriptionId(id);
        amendment.setContractEffectiveDate(formatDate(cancelSubscriptionRequest.getContractEffectiveDate()));
        amendment.setEffectiveDate(formatDate(cancelSubscriptionRequest.getEffectiveDate()));

        ZuoraServiceStub.AmendOptions amendOptions = new ZuoraServiceStub.AmendOptions();
        amendOptions.setGenerateInvoice(cancelSubscriptionRequest.isGenerateInvoice());
        amendOptions.setProcessPayments(cancelSubscriptionRequest.isProcessPayments());

        LOGGER.info("Send '{}' amendment for subscription '{}'", amendment.getType(), cancelSubscriptionRequest.getSubscriptionId());
        loginZuoraSoap();
        return zApi.zAmend(new ZuoraServiceStub.Amendment[]{amendment}, amendOptions);
    }

    //    /**
//     * @see IZuoraExternalServiceSoap#isValidZuoraIdentifier(String)
//     */
    @Override
    public boolean isValidZuoraIdentifier(final String id) {
        try {
            final ZuoraServiceStub.ID zuoraId = new ZuoraServiceStub.ID();
            zuoraId.setID(id);
            return true;
        } catch (Exception e) {
            LOGGER.warn("There is an error building ZuoraServiceStub.ID with arguments '{}'", id);
            return false;
        }
    }

    /**
     * Do soap query to zuora by select items, entity and filter given.
     *
     * @param entity
     * @param selectClause
     * @param whereClause
     * @return
     */
    private ZuoraServiceStub.QueryResult queryToZuora(final String entity, final Set<String> selectClause, final Map<String, String> whereClause) {
        final List<String> where = whereClause.entrySet().stream().map(item -> String.format(" %s = '%s' ", item.getKey(), item.getValue())).collect(Collectors.toList());
        final String query = String.format("SELECT %s FROM %s WHERE %s ", String.join(", ", selectClause), entity, String.join(" AND ", where));
        LOGGER.debug("query for Zuora: {}", query);
        return zApi.zQuery(query);
    }

    public List queryToZuoraAllRecords(final String entity, final Set<String> selectClause, final Map<String, String> whereClause) {
        final List<String> where = whereClause.entrySet().stream().map(item -> String.format(" %s '%s' ", item.getKey(), item.getValue())).collect(Collectors.toList());
        final String query = String.format("SELECT %s FROM %s WHERE %s ", String.join(", ", selectClause), entity, String.join(" AND ", where));
        LOGGER.debug("query for Zuora: {}", query);
        return zApi.zAdvancedQuery(query);
    }

    public enum ZStatus {
        RENEWED, CANCELLED, ACTIVE
    }

    public enum ZAmendmentTypes {
        Cancellation, NewProduct, OwnerTransfer, RemoveProduct, Renewal, UpdateProduct, TermsAndConditions
    }

    public enum AccountZStatus {
        DRAFT("Draft"), ACTIVE("Active"), CANCELED("Canceled");

        private String value;

        private AccountZStatus(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum RefundStatus {
        CANCELED("Canceled"), ERROR("Error"), PROCESSED("Processed"), PROCESSING("Processing");

        private String value;

        private RefundStatus(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

}
