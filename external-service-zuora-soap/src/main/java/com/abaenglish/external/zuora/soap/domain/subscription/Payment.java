package com.abaenglish.external.zuora.soap.domain.subscription;

import java.io.Serializable;
import java.math.BigDecimal;

public class Payment implements Serializable {

    private static final long serialVersionUID = -8514324490629274608L;
    private String id;
    private String paymentNumber;
    private String status;
    private String accountId;
    private BigDecimal amount;
    private String cancelledOn;
    private String effectiveDate;
    private String gateway;
    private String gatewayResponse;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber(String paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCancelledOn() {
        return cancelledOn;
    }

    public void setCancelledOn(String cancelledOn) {
        this.cancelledOn = cancelledOn;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getGatewayResponse() {
        return gatewayResponse;
    }

    public void setGatewayResponse(String gatewayResponse) {
        this.gatewayResponse = gatewayResponse;
    }

    public static final class Builder {
        private String id;
        private String paymentNumber;
        private String status;
        private String accountId;
        private BigDecimal amount;
        private String cancelledOn;
        private String effectiveDate;
        private String gateway;
        private String gatewayResponse;

        private Builder() {
        }

        public static Builder aPayment() {
            return new Builder();
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder paymentNumber(String paymentNumber) {
            this.paymentNumber = paymentNumber;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder accountId(String accountId) {
            this.accountId = accountId;
            return this;
        }

        public Builder amount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public Builder cancelledOn(String cancelledOn) {
            this.cancelledOn = cancelledOn;
            return this;
        }

        public Builder effectiveDate(String effectiveDate) {
            this.effectiveDate = effectiveDate;
            return this;
        }

        public Builder gateway(String gateway) {
            this.gateway = gateway;
            return this;
        }

        public Builder gatewayResponse(String gatewayResponse) {
            this.gatewayResponse = gatewayResponse;
            return this;
        }

        public Payment build() {
            Payment payment = new Payment();
            payment.setId(id);
            payment.setPaymentNumber(paymentNumber);
            payment.setStatus(status);
            payment.setAccountId(accountId);
            payment.setAmount(amount);
            payment.setCancelledOn(cancelledOn);
            payment.setEffectiveDate(effectiveDate);
            payment.setGateway(gateway);
            payment.setGatewayResponse(gatewayResponse);
            return payment;
        }
    }
}
