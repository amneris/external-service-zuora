package com.abaenglish.external.zuora.soap.domain.subscription;

import java.math.BigDecimal;

public class ZuoraRefundResponse {

    private String id;
    private String accountId;
    private BigDecimal amount;
    private String refundDate;
    private String updatedDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRefundDate() {
        return refundDate;
    }

    public void setRefundDate(String refundDate) {
        this.refundDate = refundDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public static final class Builder {
        private String id;
        private String accountId;
        private BigDecimal amount;
        private String refundDate;
        private String updatedDate;

        private Builder() {
        }

        public static Builder aRefundInfo() {
            return new Builder();
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder accountId(String accountId) {
            this.accountId = accountId;
            return this;
        }

        public Builder amount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public Builder refundDate(String refundDate) {
            this.refundDate = refundDate;
            return this;
        }

        public Builder updatedDate(String updatedDate) {
            this.updatedDate = updatedDate;
            return this;
        }

        public ZuoraRefundResponse build() {
            ZuoraRefundResponse zuoraRefundResponse = new ZuoraRefundResponse();
            zuoraRefundResponse.setId(id);
            zuoraRefundResponse.setAccountId(accountId);
            zuoraRefundResponse.setAmount(amount);
            zuoraRefundResponse.setRefundDate(refundDate);
            zuoraRefundResponse.setUpdatedDate(updatedDate);
            return zuoraRefundResponse;
        }
    }
}
