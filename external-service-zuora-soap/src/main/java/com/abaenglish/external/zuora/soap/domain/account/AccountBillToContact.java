package com.abaenglish.external.zuora.soap.domain.account;

public class AccountBillToContact {

    private String address1;

    private String address2;

    private String city;

    private String country;

    private String county;

    private String fax;

    private String firstName;

    private String lastName;

    private String nickname;

    private String homePhone;

    private String mobilePhone;

    private String otherPhone;

    private String otherPhoneType;

    private String personalEmail;

    private String state;

    private String taxRegion;

    private String workEmail;

    private String workPhone;

    private String zipCode;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getOtherPhone() {
        return otherPhone;
    }

    public void setOtherPhone(String otherPhone) {
        this.otherPhone = otherPhone;
    }

    public String getOtherPhoneType() {
        return otherPhoneType;
    }

    public void setOtherPhoneType(String otherPhoneType) {
        this.otherPhoneType = otherPhoneType;
    }

    public String getPersonalEmail() {
        return personalEmail;
    }

    public void setPersonalEmail(String personalEmail) {
        this.personalEmail = personalEmail;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTaxRegion() {
        return taxRegion;
    }

    public void setTaxRegion(String taxRegion) {
        this.taxRegion = taxRegion;
    }

    public String getWorkEmail() {
        return workEmail;
    }

    public void setWorkEmail(String workEmail) {
        this.workEmail = workEmail;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public static final class Builder {
        private String address1;
        private String address2;
        private String city;
        private String country;
        private String county;
        private String fax;
        private String firstName;
        private String lastName;
        private String nickname;
        private String homePhone;
        private String mobilePhone;
        private String otherPhone;
        private String otherPhoneType;
        private String personalEmail;
        private String state;
        private String taxRegion;
        private String workEmail;
        private String workPhone;
        private String zipCode;

        private Builder() {
        }

        public static Builder anAccountBillToContact() {
            return new Builder();
        }

        public Builder address1(String address1) {
            this.address1 = address1;
            return this;
        }

        public Builder address2(String address2) {
            this.address2 = address2;
            return this;
        }

        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public Builder country(String country) {
            this.country = country;
            return this;
        }

        public Builder county(String county) {
            this.county = county;
            return this;
        }

        public Builder fax(String fax) {
            this.fax = fax;
            return this;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder nickname(String nickname) {
            this.nickname = nickname;
            return this;
        }

        public Builder homePhone(String homePhone) {
            this.homePhone = homePhone;
            return this;
        }

        public Builder mobilePhone(String mobilePhone) {
            this.mobilePhone = mobilePhone;
            return this;
        }

        public Builder otherPhone(String otherPhone) {
            this.otherPhone = otherPhone;
            return this;
        }

        public Builder otherPhoneType(String otherPhoneType) {
            this.otherPhoneType = otherPhoneType;
            return this;
        }

        public Builder personalEmail(String personalEmail) {
            this.personalEmail = personalEmail;
            return this;
        }

        public Builder state(String state) {
            this.state = state;
            return this;
        }

        public Builder taxRegion(String taxRegion) {
            this.taxRegion = taxRegion;
            return this;
        }

        public Builder workEmail(String workEmail) {
            this.workEmail = workEmail;
            return this;
        }

        public Builder workPhone(String workPhone) {
            this.workPhone = workPhone;
            return this;
        }

        public Builder zipCode(String zipCode) {
            this.zipCode = zipCode;
            return this;
        }

        public AccountBillToContact build() {
            AccountBillToContact accountBillToContact = new AccountBillToContact();
            accountBillToContact.setAddress1(address1);
            accountBillToContact.setAddress2(address2);
            accountBillToContact.setCity(city);
            accountBillToContact.setCountry(country);
            accountBillToContact.setCounty(county);
            accountBillToContact.setFax(fax);
            accountBillToContact.setFirstName(firstName);
            accountBillToContact.setLastName(lastName);
            accountBillToContact.setNickname(nickname);
            accountBillToContact.setHomePhone(homePhone);
            accountBillToContact.setMobilePhone(mobilePhone);
            accountBillToContact.setOtherPhone(otherPhone);
            accountBillToContact.setOtherPhoneType(otherPhoneType);
            accountBillToContact.setPersonalEmail(personalEmail);
            accountBillToContact.setState(state);
            accountBillToContact.setTaxRegion(taxRegion);
            accountBillToContact.setWorkEmail(workEmail);
            accountBillToContact.setWorkPhone(workPhone);
            accountBillToContact.setZipCode(zipCode);
            return accountBillToContact;
        }
    }
}