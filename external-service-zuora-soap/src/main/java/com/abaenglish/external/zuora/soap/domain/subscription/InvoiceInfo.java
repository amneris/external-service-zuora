package com.abaenglish.external.zuora.soap.domain.subscription;

import java.math.BigDecimal;
import java.util.Date;

public class InvoiceInfo {

    private String id;
    private String invoiceNumber;
    private BigDecimal amount;
    private Date dueDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public static final class Builder {
        private String id;
        private String invoiceNumber;
        private BigDecimal amount;
        private Date dueDate;

        private Builder() {
        }

        public static Builder anInvoiceInfo() {
            return new Builder();
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder dueDate(Date dueDate) {
            this.dueDate = dueDate;
            return this;
        }
        public Builder invoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public Builder amount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public InvoiceInfo build() {
            InvoiceInfo invoiceInfo = new InvoiceInfo();
            invoiceInfo.setId(id);
            invoiceInfo.setInvoiceNumber(invoiceNumber);
            invoiceInfo.setAmount(amount);
            invoiceInfo.setDueDate(dueDate);
            return invoiceInfo;
        }
    }
}