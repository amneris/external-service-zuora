package com.abaenglish.external.zuora.soap.domain.subscription;

import java.io.Serializable;
import java.math.BigDecimal;

public class InvoiceItemsInfo implements Serializable {

    private static final long serialVersionUID = 7489513313699214364L;
    private String id;
    private String productId;
    private String productName;
    private String accountingCode;
    private String appliedToInvoiceItemId;
    private BigDecimal chargeAmount;
    private String chargeName;
    private String chargeDescription;
    private String chargeType;
    private String invoiceId;
    private Integer quantity;
    private String ratePlanChargeId;
    private String ratePlanId;
    private BigDecimal taxAmount;
    private String taxCode;
    private BigDecimal taxExemptAmount;
    private String taxMode;
    private BigDecimal unitPrice;
    private String SKU;
    private String subscriptionId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getAccountingCode() {
        return accountingCode;
    }

    public void setAccountingCode(String accountingCode) {
        this.accountingCode = accountingCode;
    }

    public String getAppliedToInvoiceItemId() {
        return appliedToInvoiceItemId;
    }

    public void setAppliedToInvoiceItemId(String appliedToInvoiceItemId) {
        this.appliedToInvoiceItemId = appliedToInvoiceItemId;
    }

    public BigDecimal getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(BigDecimal chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public String getChargeName() {
        return chargeName;
    }

    public void setChargeName(String chargeName) {
        this.chargeName = chargeName;
    }

    public String getChargeDescription() {
        return chargeDescription;
    }

    public void setChargeDescription(String chargeDescription) {
        this.chargeDescription = chargeDescription;
    }

    public String getChargeType() {
        return chargeType;
    }

    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getRatePlanChargeId() {
        return ratePlanChargeId;
    }

    public void setRatePlanChargeId(String ratePlanChargeId) {
        this.ratePlanChargeId = ratePlanChargeId;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public BigDecimal getTaxExemptAmount() {
        return taxExemptAmount;
    }

    public void setTaxExemptAmount(BigDecimal taxExemptAmount) {
        this.taxExemptAmount = taxExemptAmount;
    }

    public String getTaxMode() {
        return taxMode;
    }

    public void setTaxMode(String taxMode) {
        this.taxMode = taxMode;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getSKU() {
        return SKU;
    }

    public void setSKU(String SKU) {
        this.SKU = SKU;
    }

    public String getRatePlanId() {
        return ratePlanId;
    }

    public void setRatePlanId(String ratePlanId) {
        this.ratePlanId = ratePlanId;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public static final class Builder {
        private String id;
        private String productId;
        private String productName;
        private String accountingCode;
        private String appliedToInvoiceItemId;
        private BigDecimal chargeAmount;
        private String chargeName;
        private String chargeDescription;
        private String chargeType;
        private String invoiceId;
        private Integer quantity;
        private String ratePlanChargeId;
        private BigDecimal taxAmount;
        private String taxCode;
        private BigDecimal taxExemptAmount;
        private String taxMode;
        private BigDecimal unitPrice;
        private String SKU;
        private String subscriptionId;

        private Builder() {
        }

        public static Builder anInvoiceItemsInfo() {
            return new Builder();
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder productId(String productId) {
            this.productId = productId;
            return this;
        }

        public Builder productName(String productName) {
            this.productName = productName;
            return this;
        }

        public Builder accountingCode(String accountingCode) {
            this.accountingCode = accountingCode;
            return this;
        }

        public Builder appliedToInvoiceItemId(String appliedToInvoiceItemId) {
            this.appliedToInvoiceItemId = appliedToInvoiceItemId;
            return this;
        }

        public Builder chargeAmount(BigDecimal chargeAmount) {
            this.chargeAmount = chargeAmount;
            return this;
        }

        public Builder chargeName(String chargeName) {
            this.chargeName = chargeName;
            return this;
        }

        public Builder chargeDescription(String chargeDescription) {
            this.chargeDescription = chargeDescription;
            return this;
        }

        public Builder chargeType(String chargeType) {
            this.chargeType = chargeType;
            return this;
        }

        public Builder invoiceId(String invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        public Builder quantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public Builder ratePlanChargeId(String ratePlanChargeId) {
            this.ratePlanChargeId = ratePlanChargeId;
            return this;
        }

        public Builder taxAmount(BigDecimal taxAmount) {
            this.taxAmount = taxAmount;
            return this;
        }

        public Builder taxCode(String taxCode) {
            this.taxCode = taxCode;
            return this;
        }

        public Builder taxExemptAmount(BigDecimal taxExemptAmount) {
            this.taxExemptAmount = taxExemptAmount;
            return this;
        }

        public Builder taxMode(String taxMode) {
            this.taxMode = taxMode;
            return this;
        }

        public Builder unitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
            return this;
        }

        public Builder SKU(String SKU) {
            this.SKU = SKU;
            return this;
        }

        public Builder subscriptionId(String subscriptionId) {
            this.subscriptionId = subscriptionId;
            return this;
        }

        public InvoiceItemsInfo build() {
            InvoiceItemsInfo invoiceItemsInfo = new InvoiceItemsInfo();
            invoiceItemsInfo.setId(id);
            invoiceItemsInfo.setProductId(productId);
            invoiceItemsInfo.setProductName(productName);
            invoiceItemsInfo.setAccountingCode(accountingCode);
            invoiceItemsInfo.setAppliedToInvoiceItemId(appliedToInvoiceItemId);
            invoiceItemsInfo.setChargeAmount(chargeAmount);
            invoiceItemsInfo.setChargeName(chargeName);
            invoiceItemsInfo.setChargeDescription(chargeDescription);
            invoiceItemsInfo.setChargeType(chargeType);
            invoiceItemsInfo.setInvoiceId(invoiceId);
            invoiceItemsInfo.setQuantity(quantity);
            invoiceItemsInfo.setRatePlanChargeId(ratePlanChargeId);
            invoiceItemsInfo.setTaxAmount(taxAmount);
            invoiceItemsInfo.setTaxCode(taxCode);
            invoiceItemsInfo.setTaxExemptAmount(taxExemptAmount);
            invoiceItemsInfo.setTaxMode(taxMode);
            invoiceItemsInfo.setUnitPrice(unitPrice);
            invoiceItemsInfo.setSKU(SKU);
            invoiceItemsInfo.setSubscriptionId(subscriptionId);
            return invoiceItemsInfo;
        }
    }
}