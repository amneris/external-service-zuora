package com.abaenglish.external.zuora.soap.service.dto.subscription;

import java.io.Serializable;

public class ZuoraAmendmentResponse implements Serializable {

    private static final long serialVersionUID = 792471608372502127L;
    private String id;
    private String subscriptionId;
    private String updatedDate;
    private String code;
    private String status;
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static final class Builder {
        private String id;
        private String subscriptionId;
        private String updatedDate;
        private String code;
        private String status;
        private String type;

        private Builder() {
        }

        public static Builder aZuoraAmendmentResponse() {
            return new Builder();
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder subscriptionId(String subscriptionId) {
            this.subscriptionId = subscriptionId;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder updatedDate(String updatedDate) {
            this.updatedDate = updatedDate;
            return this;
        }

        public Builder code(String code) {
            this.code = code;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public ZuoraAmendmentResponse build() {
            ZuoraAmendmentResponse zuoraAmendmentResponse = new ZuoraAmendmentResponse();
            zuoraAmendmentResponse.setId(id);
            zuoraAmendmentResponse.setSubscriptionId(subscriptionId);
            zuoraAmendmentResponse.setUpdatedDate(updatedDate);
            zuoraAmendmentResponse.setCode(code);
            zuoraAmendmentResponse.setStatus(status);
            zuoraAmendmentResponse.setType(type);
            return zuoraAmendmentResponse;
        }
    }
}
