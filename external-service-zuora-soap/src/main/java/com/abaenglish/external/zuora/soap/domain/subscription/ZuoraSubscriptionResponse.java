package com.abaenglish.external.zuora.soap.domain.subscription;

import java.io.Serializable;

public class ZuoraSubscriptionResponse implements Serializable {

    private static final long serialVersionUID = -5345730230340990774L;

    private String id;
    private String originalId;
    private String accountId;
    private String termStartDate;
    private String termEndDate;
    private String cancelledDate;
    private String previousSubscriptionId;
    private String subscriptionStartDate;
    private String subscriptionEndDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginalId() {
        return originalId;
    }

    public void setOriginalId(String originalId) {
        this.originalId = originalId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getTermStartDate() {
        return termStartDate;
    }

    public void setTermStartDate(String termStartDate) {
        this.termStartDate = termStartDate;
    }

    public String getTermEndDate() {
        return termEndDate;
    }

    public void setTermEndDate(String termEndDate) {
        this.termEndDate = termEndDate;
    }

    public String getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(String cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public String getPreviousSubscriptionId() {
        return previousSubscriptionId;
    }

    public void setPreviousSubscriptionId(String previousSubscriptionId) {
        this.previousSubscriptionId = previousSubscriptionId;
    }

    public String getSubscriptionStartDate() {
        return subscriptionStartDate;
    }

    public void setSubscriptionStartDate(String subscriptionStartDate) {
        this.subscriptionStartDate = subscriptionStartDate;
    }

    public String getSubscriptionEndDate() {
        return subscriptionEndDate;
    }

    public void setSubscriptionEndDate(String subscriptionEndDate) {
        this.subscriptionEndDate = subscriptionEndDate;
    }

    public static final class Builder {
        private String id;
        private String originalId;
        private String accountId;
        private String termStartDate;
        private String termEndDate;
        private String cancelledDate;
        private String previousSubscriptionId;
        private String subscriptionStartDate;
        private String subscriptionEndDate;

        private Builder() {
        }

        public static Builder aZuoraSubscriptionResponse() {
            return new Builder();
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder originalId(String originalId) {
            this.originalId = originalId;
            return this;
        }

        public Builder accountId(String accountId) {
            this.accountId = accountId;
            return this;
        }

        public Builder termStartDate(String termStartDate) {
            this.termStartDate = termStartDate;
            return this;
        }

        public Builder termEndDate(String termEndDate) {
            this.termEndDate = termEndDate;
            return this;
        }

        public Builder cancelledDate(String cancelledDate) {
            this.cancelledDate = cancelledDate;
            return this;
        }

        public Builder previousSubscriptionId(String previousSubscriptionId) {
            this.previousSubscriptionId = previousSubscriptionId;
            return this;
        }

        public Builder subscriptionStartDate(String subscriptionStartDate) {
            this.subscriptionStartDate = subscriptionStartDate;
            return this;
        }

        public Builder subscriptionEndDate(String subscriptionEndDate) {
            this.subscriptionEndDate = subscriptionEndDate;
            return this;
        }

        public ZuoraSubscriptionResponse build() {
            ZuoraSubscriptionResponse zuoraSubscriptionResponse = new ZuoraSubscriptionResponse();
            zuoraSubscriptionResponse.setId(id);
            zuoraSubscriptionResponse.setOriginalId(originalId);
            zuoraSubscriptionResponse.setAccountId(accountId);
            zuoraSubscriptionResponse.setTermStartDate(termStartDate);
            zuoraSubscriptionResponse.setTermEndDate(termEndDate);
            zuoraSubscriptionResponse.setCancelledDate(cancelledDate);
            zuoraSubscriptionResponse.setPreviousSubscriptionId(previousSubscriptionId);
            zuoraSubscriptionResponse.setSubscriptionStartDate(subscriptionStartDate);
            zuoraSubscriptionResponse.setSubscriptionEndDate(subscriptionEndDate);
            return zuoraSubscriptionResponse;
        }
    }
}
