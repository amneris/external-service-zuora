package com.abaenglish.external.zuora.soap.autoconfigure;

import com.abaenglish.external.zuora.soap.properties.ZuoraSoapProperties;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import org.springframework.context.annotation.Import;

@Import({ZuoraSoapProperties.class, ZuoraExternalServiceSoap.class})
public class ExternalServiceZuoraAutoConfiguration {
}
