package com.abaenglish.external.zuora.soap.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "external.zuora")
public class ZuoraSoapProperties {

    private Soap soap;
    private boolean accountInvoiceDeliveryPrefsEmail;

    public static class Soap{
        private String username;
        private String password;
        private String host;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }
    }

    public Soap getSoap() {
        return soap;
    }

    public void setSoap(Soap soap) {
        this.soap = soap;
    }

    public boolean isAccountInvoiceDeliveryPrefsEmail() {
        return accountInvoiceDeliveryPrefsEmail;
    }

    public void setAccountInvoiceDeliveryPrefsEmail(boolean accountInvoiceDeliveryPrefsEmail) {
        this.accountInvoiceDeliveryPrefsEmail = accountInvoiceDeliveryPrefsEmail;
    }
}