package com.abaenglish.external.zuora.soap.service.dto;

import java.util.List;

public class CreateSubscriptionRequest {

    private Long userId;
    private String name;
    private String surname;
    private String country;
    private String currency;
    private String email;

    private String paymentId;
    private List<String> ratePlanId;
    private Integer period;

    private String zuoraAccountId;
    private String zuoraAccountNumber;

    private String type;
    private String paypalBaid;
    private String paypalEmail;

    private String gatewayName;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public List<String> getRatePlanId() {
        return ratePlanId;
    }

    public void setRatePlanId(List<String> ratePlanId) {
        this.ratePlanId = ratePlanId;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getZuoraAccountId() {
        return zuoraAccountId;
    }

    public void setZuoraAccountId(String zuoraAccountId) {
        this.zuoraAccountId = zuoraAccountId;
    }

    public String getZuoraAccountNumber() {
        return zuoraAccountNumber;
    }

    public void setZuoraAccountNumber(String zuoraAccountNumber) {
        this.zuoraAccountNumber = zuoraAccountNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPaypalBaid() {
        return paypalBaid;
    }

    public void setPaypalBaid(String paypalBaid) {
        this.paypalBaid = paypalBaid;
    }

    public String getPaypalEmail() {
        return paypalEmail;
    }

    public void setPaypalEmail(String paypalEmail) {
        this.paypalEmail = paypalEmail;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public static final class Builder {
        private Long userId;
        private String name;
        private String surname;
        private String country;
        private String currency;
        private String email;
        private String paymentId;
        private List<String> ratePlanId;
        private Integer period;
        private String zuoraAccountId;
        private String zuoraAccountNumber;
        private String type;
        private String paypalBaid;
        private String paypalEmail;
        private String gatewayName;

        private Builder() {
        }

        public static Builder aCreateSubscriptionRequest() {
            return new Builder();
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder surname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder country(String country) {
            this.country = country;
            return this;
        }

        public Builder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder paymentId(String paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public Builder ratePlanId(List<String> ratePlanId) {
            this.ratePlanId = ratePlanId;
            return this;
        }

        public Builder period(Integer period) {
            this.period = period;
            return this;
        }

        public Builder zuoraAccountId(String zuoraAccountId) {
            this.zuoraAccountId = zuoraAccountId;
            return this;
        }

        public Builder zuoraAccountNumber(String zuoraAccountNumber) {
            this.zuoraAccountNumber = zuoraAccountNumber;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder paypalBaid(String paypalBaid) {
            this.paypalBaid = paypalBaid;
            return this;
        }

        public Builder paypalEmail(String paypalEmail) {
            this.paypalEmail = paypalEmail;
            return this;
        }

        public Builder gatewayName(String gatewayName) {
            this.gatewayName = gatewayName;
            return this;
        }

        public CreateSubscriptionRequest build() {
            CreateSubscriptionRequest createSubscriptionRequest = new CreateSubscriptionRequest();
            createSubscriptionRequest.setUserId(userId);
            createSubscriptionRequest.setName(name);
            createSubscriptionRequest.setSurname(surname);
            createSubscriptionRequest.setCountry(country);
            createSubscriptionRequest.setCurrency(currency);
            createSubscriptionRequest.setEmail(email);
            createSubscriptionRequest.setPaymentId(paymentId);
            createSubscriptionRequest.setRatePlanId(ratePlanId);
            createSubscriptionRequest.setPeriod(period);
            createSubscriptionRequest.setZuoraAccountId(zuoraAccountId);
            createSubscriptionRequest.setZuoraAccountNumber(zuoraAccountNumber);
            createSubscriptionRequest.setType(type);
            createSubscriptionRequest.setPaypalBaid(paypalBaid);
            createSubscriptionRequest.setPaypalEmail(paypalEmail);
            createSubscriptionRequest.setGatewayName(gatewayName);
            return createSubscriptionRequest;
        }
    }
}
