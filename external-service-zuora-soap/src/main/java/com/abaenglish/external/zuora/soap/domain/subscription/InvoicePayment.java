package com.abaenglish.external.zuora.soap.domain.subscription;

import java.io.Serializable;

public class InvoicePayment implements Serializable {

    private static final long serialVersionUID = -8366462169490772939L;
    private String id;
    private String invoiceId;
    private String paymentId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public static final class Builder {
        private String id;
        private String invoiceId;
        private String paymentId;

        private Builder() {
        }

        public static Builder anInvoicePayment() {
            return new Builder();
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder invoiceId(String invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        public Builder paymentId(String paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public InvoicePayment build() {
            InvoicePayment invoicePayment = new InvoicePayment();
            invoicePayment.setId(id);
            invoicePayment.setInvoiceId(invoiceId);
            invoicePayment.setPaymentId(paymentId);
            return invoicePayment;
        }
    }
}