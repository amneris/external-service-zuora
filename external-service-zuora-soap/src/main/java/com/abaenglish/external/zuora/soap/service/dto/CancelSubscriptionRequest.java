package com.abaenglish.external.zuora.soap.service.dto;

import java.io.Serializable;
import java.util.Date;

public class CancelSubscriptionRequest implements Serializable {

    private static final long serialVersionUID = -5062599725190737810L;

    private String subscriptionId;
    private Date contractEffectiveDate;
    private Date effectiveDate;
    private String name;
    private boolean generateInvoice;
    private boolean processPayments;

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Date getContractEffectiveDate() {
        return contractEffectiveDate;
    }

    public void setContractEffectiveDate(Date contractEffectiveDate) {
        this.contractEffectiveDate = contractEffectiveDate;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGenerateInvoice() {
        return generateInvoice;
    }

    public void setGenerateInvoice(boolean generateInvoice) {
        this.generateInvoice = generateInvoice;
    }

    public boolean isProcessPayments() {
        return processPayments;
    }

    public void setProcessPayments(boolean processPayments) {
        this.processPayments = processPayments;
    }

    public static final class Builder {
        private String subscriptionId;
        private Date contractEffectiveDate;
        private Date effectiveDate;
        private String name;
        private boolean generateInvoice;
        private boolean processPayments;

        private Builder() {
        }

        public static Builder aCancelSubscriptionRequest() {
            return new Builder();
        }

        public Builder subscriptionId(String subscriptionId) {
            this.subscriptionId = subscriptionId;
            return this;
        }

        public Builder contractEffectiveDate(Date contractEffectiveDate) {
            this.contractEffectiveDate = contractEffectiveDate;
            return this;
        }

        public Builder effectiveDate(Date effectiveDate) {
            this.effectiveDate = effectiveDate;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder generateInvoice(boolean generateInvoice) {
            this.generateInvoice = generateInvoice;
            return this;
        }

        public Builder processPayments(boolean processPayments) {
            this.processPayments = processPayments;
            return this;
        }

        public CancelSubscriptionRequest build() {
            CancelSubscriptionRequest cancelSubscriptionRequest = new CancelSubscriptionRequest();
            cancelSubscriptionRequest.setSubscriptionId(subscriptionId);
            cancelSubscriptionRequest.setContractEffectiveDate(contractEffectiveDate);
            cancelSubscriptionRequest.setEffectiveDate(effectiveDate);
            cancelSubscriptionRequest.setName(name);
            cancelSubscriptionRequest.setGenerateInvoice(generateInvoice);
            cancelSubscriptionRequest.setProcessPayments(processPayments);
            return cancelSubscriptionRequest;
        }
    }
}

