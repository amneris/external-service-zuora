package com.abaenglish.external.zuora.rest.domain.signature;

public class ResponseSignature {

    private String signature;
    private String token;
    private String tenantId;
    private String key;
    private boolean success;

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static final class Builder {
        private String signature;
        private String token;
        private String tenantId;
        private String key;
        private boolean success;

        private Builder() {
        }

        public static Builder aResponseSignature() {
            return new Builder();
        }

        public Builder signature(String signature) {
            this.signature = signature;
            return this;
        }

        public Builder token(String token) {
            this.token = token;
            return this;
        }

        public Builder tenantId(String tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public Builder key(String key) {
            this.key = key;
            return this;
        }

        public Builder success(boolean success) {
            this.success = success;
            return this;
        }

        public ResponseSignature build() {
            ResponseSignature responseSignature = new ResponseSignature();
            responseSignature.setSignature(signature);
            responseSignature.setToken(token);
            responseSignature.setTenantId(tenantId);
            responseSignature.setKey(key);
            responseSignature.setSuccess(success);
            return responseSignature;
        }
    }

}
