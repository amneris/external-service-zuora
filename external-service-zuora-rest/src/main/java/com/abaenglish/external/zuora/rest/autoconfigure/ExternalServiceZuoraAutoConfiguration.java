package com.abaenglish.external.zuora.rest.autoconfigure;

import com.abaenglish.external.zuora.rest.properties.ZuoraRestProperties;
import com.abaenglish.external.zuora.rest.service.impl.ZuoraExternalServiceRest;
import org.springframework.context.annotation.Import;

@Import({ZuoraRestProperties.class, ZuoraExternalServiceRest.class})
public class ExternalServiceZuoraAutoConfiguration {
}
