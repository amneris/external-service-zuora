package com.abaenglish.external.zuora.rest.domain.catalog;

import java.math.BigDecimal;

public class Pricing {
    private String currency;
    private BigDecimal price;
    private BigDecimal discountPercentage;
    private BigDecimal discountAmount;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public static final class Builder {
        private String currency;
        private BigDecimal price;
        private BigDecimal discountPercentage;
        private BigDecimal discountAmount;

        private Builder() {
        }

        public static Builder aPricing() {
            return new Builder();
        }

        public Builder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public Builder price(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder discountPercentage(BigDecimal discountPercentage) {
            this.discountPercentage = discountPercentage;
            return this;
        }

        public Builder discountAmount(BigDecimal discountAmount) {
            this.discountAmount = discountAmount;
            return this;
        }

        public Pricing build() {
            Pricing pricing = new Pricing();
            pricing.setCurrency(currency);
            pricing.setPrice(price);
            pricing.setDiscountPercentage(discountPercentage);
            pricing.setDiscountAmount(discountAmount);
            return pricing;
        }
    }
}