package com.abaenglish.external.zuora.rest.domain.catalog;

import java.util.ArrayList;
import java.util.List;

public class Product {

    private String id;

    private String sku;

    private String name;

    private String description;

    private List<ProductRatePlan> productRatePlans = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductRatePlan> getProductRatePlans() {
        return productRatePlans;
    }

    public void setProductRatePlans(List<ProductRatePlan> productRatePlans) {
        this.productRatePlans = productRatePlans;
    }

    public static final class Builder {
        private String id;
        private String sku;
        private String name;
        private String description;
        private List<ProductRatePlan> productRatePlans = new ArrayList<>();

        private Builder() {
        }

        public static Builder aProduct() {
            return new Builder();
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder sku(String sku) {
            this.sku = sku;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder productRatePlans(List<ProductRatePlan> productRatePlans) {
            this.productRatePlans = productRatePlans;
            return this;
        }

        public Product build() {
            Product product = new Product();
            product.setId(id);
            product.setSku(sku);
            product.setName(name);
            product.setDescription(description);
            product.setProductRatePlans(productRatePlans);
            return product;
        }
    }
}
