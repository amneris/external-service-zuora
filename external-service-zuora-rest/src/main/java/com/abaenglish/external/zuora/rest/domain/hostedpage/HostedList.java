package com.abaenglish.external.zuora.rest.domain.hostedpage;

import java.util.ArrayList;
import java.util.List;

public class HostedList {

    private List<Hostedpage> hostedpages = new ArrayList<>();
    private Boolean success;

    public List<Hostedpage> getHostedpages() {
        return hostedpages;
    }

    public void setHostedpages(List<Hostedpage> hostedpages) {
        this.hostedpages = hostedpages;
    }

    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public static final class Builder {
        private List<Hostedpage> hostedpages = new ArrayList<>();
        private Boolean success;

        private Builder() {
        }

        public static Builder aHostedList() {
            return new Builder();
        }

        public Builder hostedpages(List<Hostedpage> hostedpages) {
            this.hostedpages = hostedpages;
            return this;
        }

        public Builder success(Boolean success) {
            this.success = success;
            return this;
        }

        public HostedList build() {
            HostedList hostedList = new HostedList();
            hostedList.setHostedpages(hostedpages);
            hostedList.setSuccess(success);
            return hostedList;
        }
    }

}
