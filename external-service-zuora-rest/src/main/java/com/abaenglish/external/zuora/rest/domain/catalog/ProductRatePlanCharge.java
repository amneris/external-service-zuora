package com.abaenglish.external.zuora.rest.domain.catalog;

import java.util.ArrayList;
import java.util.List;

public class ProductRatePlanCharge {

    private String id;
    private String name;
    private String type;
    private String model;
    private String uom;
    private List<String> pricingSummary = new ArrayList<>();
    private List<Pricing> pricing = new ArrayList<>();
    private String defaultQuantity;
    private String applyDiscountTo;
    private String discountLevel;
    private String endDateCondition;
    private String upToPeriods;
    private String upToPeriodsType;
    private String billingDay;
    private String listPriceBase;
    private String billingPeriod;
    private String billingPeriodAlignment;
    private String specificBillingPeriod;
    private String smoothingModel;
    private String numberOfPeriods;
    private String overageCalculationOption;
    private String overageUnusedUnitsCreditOption;
    private String usageRecordRatingOption;
    private String priceChangeOption;
    private String priceIncreasePercentage;
    private String useTenantDefaultForPriceChange;
    private Boolean taxable;
    private String taxCode;
    private String taxMode;
    private String triggerEvent;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public List<String> getPricingSummary() {
        return pricingSummary;
    }

    public void setPricingSummary(List<String> pricingSumary) {
        this.pricingSummary = pricingSumary;
    }

    public List<Pricing> getPricing() {
        return pricing;
    }

    public void setPricing(List<Pricing> pricing) {
        this.pricing = pricing;
    }

    public String getDefaultQuantity() {
        return defaultQuantity;
    }

    public void setDefaultQuantity(String defaultQuantity) {
        this.defaultQuantity = defaultQuantity;
    }

    public String getApplyDiscountTo() {
        return applyDiscountTo;
    }

    public void setApplyDiscountTo(String applyDiscountTo) {
        this.applyDiscountTo = applyDiscountTo;
    }

    public String getDiscountLevel() {
        return discountLevel;
    }

    public void setDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
    }

    public String getEndDateCondition() {
        return endDateCondition;
    }

    public void setEndDateCondition(String endDateCondition) {
        this.endDateCondition = endDateCondition;
    }

    public String getUpToPeriods() {
        return upToPeriods;
    }

    public void setUpToPeriods(String upToPeriods) {
        this.upToPeriods = upToPeriods;
    }

    public String getUpToPeriodsType() {
        return upToPeriodsType;
    }

    public void setUpToPeriodsType(String upToPeriodsType) {
        this.upToPeriodsType = upToPeriodsType;
    }

    public String getBillingDay() {
        return billingDay;
    }

    public void setBillingDay(String billingDay) {
        this.billingDay = billingDay;
    }

    public String getListPriceBase() {
        return listPriceBase;
    }

    public void setListPriceBase(String listPriceBase) {
        this.listPriceBase = listPriceBase;
    }

    public String getBillingPeriod() {
        return billingPeriod;
    }

    public void setBillingPeriod(String billingPeriod) {
        this.billingPeriod = billingPeriod;
    }

    public String getBillingPeriodAlignment() {
        return billingPeriodAlignment;
    }

    public void setBillingPeriodAlignment(String billingPeriodAlignment) {
        this.billingPeriodAlignment = billingPeriodAlignment;
    }

    public String getSpecificBillingPeriod() {
        return specificBillingPeriod;
    }

    public void setSpecificBillingPeriod(String specificBillingPeriod) {
        this.specificBillingPeriod = specificBillingPeriod;
    }

    public String getSmoothingModel() {
        return smoothingModel;
    }

    public void setSmoothingModel(String smoothingModel) {
        this.smoothingModel = smoothingModel;
    }

    public String getNumberOfPeriods() {
        return numberOfPeriods;
    }

    public void setNumberOfPeriods(String numberOfPeriods) {
        this.numberOfPeriods = numberOfPeriods;
    }

    public String getOverageCalculationOption() {
        return overageCalculationOption;
    }

    public void setOverageCalculationOption(String overageCalculationOption) {
        this.overageCalculationOption = overageCalculationOption;
    }

    public String getOverageUnusedUnitsCreditOption() {
        return overageUnusedUnitsCreditOption;
    }

    public void setOverageUnusedUnitsCreditOption(String overageUnusedUnitsCreditOption) {
        this.overageUnusedUnitsCreditOption = overageUnusedUnitsCreditOption;
    }

    public String getUsageRecordRatingOption() {
        return usageRecordRatingOption;
    }

    public void setUsageRecordRatingOption(String usageRecordRatingOption) {
        this.usageRecordRatingOption = usageRecordRatingOption;
    }

    public String getPriceChangeOption() {
        return priceChangeOption;
    }

    public void setPriceChangeOption(String priceChangeOption) {
        this.priceChangeOption = priceChangeOption;
    }

    public String getPriceIncreasePercentage() {
        return priceIncreasePercentage;
    }

    public void setPriceIncreasePercentage(String priceIncreasePercentage) {
        this.priceIncreasePercentage = priceIncreasePercentage;
    }

    public String getUseTenantDefaultForPriceChange() {
        return useTenantDefaultForPriceChange;
    }

    public void setUseTenantDefaultForPriceChange(String useTenantDefaultForPriceChange) {
        this.useTenantDefaultForPriceChange = useTenantDefaultForPriceChange;
    }

    public Boolean isTaxable() {
        return taxable;
    }

    public void setTaxable(Boolean taxable) {
        this.taxable = taxable;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getTaxMode() {
        return taxMode;
    }

    public void setTaxMode(String taxMode) {
        this.taxMode = taxMode;
    }

    public String getTriggerEvent() {
        return triggerEvent;
    }

    public void setTriggerEvent(String triggerEvent) {
        this.triggerEvent = triggerEvent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static final class Builder {
        private List<Pricing> pricing = new ArrayList<>();
        private String type;
        private String name;
        private String id;
        private String model;

        private Builder() {
        }

        public static Builder aProductRatePlanCharge() {
            return new Builder();
        }

        public Builder pricing(List<Pricing> pricing) {
            this.pricing = pricing;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder model(String model) {
            this.model = model;
            return this;
        }

        public ProductRatePlanCharge build() {
            ProductRatePlanCharge productRatePlanCharge = new ProductRatePlanCharge();
            productRatePlanCharge.setPricing(pricing);
            productRatePlanCharge.setType(type);
            productRatePlanCharge.setName(name);
            productRatePlanCharge.setId(id);
            productRatePlanCharge.setModel(model);
            return productRatePlanCharge;
        }
    }

}
