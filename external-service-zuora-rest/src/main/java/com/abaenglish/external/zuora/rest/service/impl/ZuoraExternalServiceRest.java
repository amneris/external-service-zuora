package com.abaenglish.external.zuora.rest.service.impl;

import com.abaenglish.boot.httprequest.BasicAuthInterceptor;
import com.abaenglish.external.zuora.rest.domain.catalog.Catalog;
import com.abaenglish.external.zuora.rest.domain.hostedpage.HostedList;
import com.abaenglish.external.zuora.rest.domain.signature.ResponseSignature;
import com.abaenglish.external.zuora.rest.domain.signature.Signature;
import com.abaenglish.external.zuora.rest.properties.ZuoraRestProperties;
import com.abaenglish.external.zuora.rest.service.IZuoraExternalServiceRest;
import com.abaenglish.external.zuora.rest.utils.HeaderInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ZuoraExternalServiceRest implements IZuoraExternalServiceRest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZuoraExternalServiceRest.class);
    @Autowired
    private ZuoraRestProperties zuoraRestProperties;
    private RestTemplate restTemplate = new RestTemplate();

    @PostConstruct
    public void addInterceptor() {
        restTemplate.setInterceptors(Arrays.asList(
                new BasicAuthInterceptor(zuoraRestProperties.getUser(), zuoraRestProperties.getPassword()),
                new HeaderInterceptor(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE),
                new HeaderInterceptor(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)));

        restTemplate.setMessageConverters(getMessageConverters());
    }

    @Override
    @Cacheable("catalog")
    public Catalog getCatalog() {
        ResponseEntity<Catalog> responseEntity =
                restTemplate.getForEntity(urlApiZuoraProxy(
                        ZuoraResourceEndpoints.GET_PRODUCT_CATALOG) + "?pageSize=99",
                        Catalog.class);

        return responseEntity.getBody();
    }

    @Override
    public HostedList getHostedList() {
        return restTemplate.getForObject(urlApiZuoraProxy(ZuoraResourceEndpoints.GET_HOSTEDPAGE), HostedList.class);
    }

    @Override
    public ResponseSignature getSignature(Signature signature) {

        String urlApi = urlApiZuoraHost(ZuoraResourceEndpoints.POST_SIGNATURE);

        return restTemplate.postForObject(urlApi, signature, ResponseSignature.class);
    }

    private List<HttpMessageConverter<?>> getMessageConverters() {
        List<HttpMessageConverter<?>> converters =
                new ArrayList<HttpMessageConverter<?>>();
        converters.add(new MappingJackson2HttpMessageConverter());
        return converters;
    }

    private String urlApiZuoraProxy(String resource) {
        return zuoraRestProperties.getProxy() + resource;
    }

    private String urlApiZuoraHost(String resource) {
        return zuoraRestProperties.getHost() + resource;
    }

}
