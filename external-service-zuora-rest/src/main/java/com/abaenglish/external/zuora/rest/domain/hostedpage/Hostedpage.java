package com.abaenglish.external.zuora.rest.domain.hostedpage;

public class Hostedpage {

    private String pageId;
    private String pageName;
    private String pageType;
    private Integer pageVersion;

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public Integer getPageVersion() {
        return pageVersion;
    }

    public void setPageVersion(Integer pageVersion) {
        this.pageVersion = pageVersion;
    }

    public static final class Builder {
        private String pageId;
        private String pageName;
        private String pageType;
        private Integer pageVersion;

        private Builder() {
        }

        public static Builder aHostedpage() {
            return new Builder();
        }

        public Builder pageId(String pageId) {
            this.pageId = pageId;
            return this;
        }

        public Builder pageName(String pageName) {
            this.pageName = pageName;
            return this;
        }

        public Builder pageType(String pageType) {
            this.pageType = pageType;
            return this;
        }

        public Builder pageVersion(Integer pageVersion) {
            this.pageVersion = pageVersion;
            return this;
        }

        public Hostedpage build() {
            Hostedpage hostedpage = new Hostedpage();
            hostedpage.setPageId(pageId);
            hostedpage.setPageName(pageName);
            hostedpage.setPageType(pageType);
            hostedpage.setPageVersion(pageVersion);
            return hostedpage;
        }
    }

}
