package com.abaenglish.external.zuora.rest.domain.catalog;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ProductRatePlan {

    private String id;

    private String status;

    private String name;

    private String description;

    private String effectiveStartDate;

    private String effectiveEndDate;

    @JsonProperty("titleKey__c")
    private String key;

    @JsonProperty("featured__c")
    private Boolean featured;

    @JsonProperty("period__c")
    private String period;

    private List<ProductRatePlanCharge> productRatePlanCharges = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Boolean getFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    public List<ProductRatePlanCharge> getRatePlanCharges() {
        return productRatePlanCharges;
    }

    public void setProductRatePlanCharges(List<ProductRatePlanCharge> productRatePlanCharges) {
        this.productRatePlanCharges = productRatePlanCharges;
    }

    public static final class Builder {
        private String id;
        private String status;
        private String name;
        private String description;
        private String effectiveStartDate;
        private String effectiveEndDate;
        private String key;
        private Boolean featured;
        private String period;
        private List<ProductRatePlanCharge> productRatePlanCharges = new ArrayList<>();

        private Builder() {
        }

        public static Builder aProductRatePlan() {
            return new Builder();
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder effectiveStartDate(String effectiveStartDate) {
            this.effectiveStartDate = effectiveStartDate;
            return this;
        }

        public Builder effectiveEndDate(String effectiveEndDate) {
            this.effectiveEndDate = effectiveEndDate;
            return this;
        }

        public Builder key(String key) {
            this.key = key;
            return this;
        }

        public Builder featured(Boolean featured) {
            this.featured = featured;
            return this;
        }

        public Builder period(String period) {
            this.period = period;
            return this;
        }

        public Builder productRatePlanCharges(List<ProductRatePlanCharge> productRatePlanCharges) {
            this.productRatePlanCharges = productRatePlanCharges;
            return this;
        }

        public ProductRatePlan build() {
            ProductRatePlan productRatePlan = new ProductRatePlan();
            productRatePlan.setId(id);
            productRatePlan.setStatus(status);
            productRatePlan.setName(name);
            productRatePlan.setDescription(description);
            productRatePlan.setEffectiveStartDate(effectiveStartDate);
            productRatePlan.setEffectiveEndDate(effectiveEndDate);
            productRatePlan.setKey(key);
            productRatePlan.setFeatured(featured);
            productRatePlan.setPeriod(period);
            productRatePlan.setProductRatePlanCharges(productRatePlanCharges);
            return productRatePlan;
        }
    }
}
