package com.abaenglish.external.zuora.rest.domain.signature;

import java.io.Serializable;

public class Signature implements Serializable {

    private String uri;
    private String method;
    private String pageId;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public static final class Builder {
        private String uri;
        private String method;
        private String pageId;

        private Builder() {
        }

        public static Builder aSignature() {
            return new Builder();
        }

        public Builder witUri(String uri) {
            this.uri = uri;
            return this;
        }

        public Builder witMethod(String method) {
            this.method = method;
            return this;
        }

        public Builder witPageId(String pageId) {
            this.pageId = pageId;
            return this;
        }

        public Signature build() {
            Signature signature = new Signature();
            signature.setUri(uri);
            signature.setMethod(method);
            signature.setPageId(pageId);
            return signature;
        }
    }

}