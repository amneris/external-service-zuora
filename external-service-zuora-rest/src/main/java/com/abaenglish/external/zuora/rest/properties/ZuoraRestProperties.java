package com.abaenglish.external.zuora.rest.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "external.zuora")
public class ZuoraRestProperties {

    private String host;
    private String proxy;
    private String user;
    private String password;
    private String hostSignature;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getProxy() {
        return proxy;
    }

    public void setProxy(String proxy) {
        this.proxy = proxy;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHostSignature() { return hostSignature; }

    public void setHostSignature(String hostSignature) { this.hostSignature = hostSignature; }
}
