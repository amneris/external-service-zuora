package com.abaenglish.external.zuora.rest.domain.catalog;

import java.util.List;

/* GET CATALOG WITH ALL PRODUCTS */

public class Catalog {

    private Boolean success;

    private List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public static final class Builder {
        private Boolean success;
        private List<Product> products;

        private Builder() {
        }

        public static Builder aCatalog() {
            return new Builder();
        }

        public Builder success(Boolean success) {
            this.success = success;
            return this;
        }

        public Builder products(List<Product> products) {
            this.products = products;
            return this;
        }

        public Catalog build() {
            Catalog catalog = new Catalog();
            catalog.setSuccess(success);
            catalog.setProducts(products);
            return catalog;
        }
    }

}
