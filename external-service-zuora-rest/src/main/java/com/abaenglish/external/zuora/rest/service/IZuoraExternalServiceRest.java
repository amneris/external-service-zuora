package com.abaenglish.external.zuora.rest.service;

import com.abaenglish.external.zuora.rest.domain.catalog.Catalog;
import com.abaenglish.external.zuora.rest.domain.hostedpage.HostedList;
import com.abaenglish.external.zuora.rest.domain.signature.ResponseSignature;
import com.abaenglish.external.zuora.rest.domain.signature.Signature;

public interface IZuoraExternalServiceRest {

    Catalog getCatalog();

    HostedList getHostedList();

    ResponseSignature getSignature(Signature signature);
}
