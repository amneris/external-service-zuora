package com.abaenglish.external.zuora.rest.objectmother;

import com.abaenglish.external.zuora.rest.domain.hostedpage.HostedList;

public class HostedListObjectMother {

    public static HostedList getHostedList() {

        HostedList hostedList = HostedList.Builder.aHostedList()
                .hostedpages(HostedpagesObjectMother.hostedpages())
                .success(true)
                .build();

        return hostedList;
    }
}
