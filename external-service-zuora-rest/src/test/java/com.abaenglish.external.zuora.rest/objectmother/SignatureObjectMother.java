package com.abaenglish.external.zuora.rest.objectmother;

import com.abaenglish.external.zuora.rest.domain.signature.ResponseSignature;
import com.abaenglish.external.zuora.rest.domain.signature.Signature;

public class SignatureObjectMother {

    public static Signature requestSignature() {

        Signature request = Signature.Builder.aSignature()
                .witMethod("POST")
                .witUri("https://apisandbox.zuora.com/apps/PublicHostedPageLite.do")
                .witPageId("2c92c0f854a35e960154a9668350793a")
                .build();

        return request;
    }

    public static ResponseSignature responseSignature() {

        ResponseSignature response = ResponseSignature.Builder.aResponseSignature()
                .key("THISISMYKEY")
                .signature("SIGNATURE123465789")
                .success(true)
                .tenantId("12345")
                .token("6tEaYYYG6HuYhqF5LtqZ0NrI2JmGZS2g")
                .build();
        return response;
    }

}
