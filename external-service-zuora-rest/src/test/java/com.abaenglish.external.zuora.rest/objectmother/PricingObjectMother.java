package com.abaenglish.external.zuora.rest.objectmother;

import com.abaenglish.external.zuora.rest.domain.catalog.Pricing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PricingObjectMother {

    public static List<Pricing> getPricings() {

        Pricing pricingEUR = Pricing.Builder.aPricing()
                .currency("EUR")
                .price(new BigDecimal(10))
                .build();

        Pricing pricingUSD = Pricing.Builder.aPricing()
                .currency("USD")
                .price(new BigDecimal(20))
                .build();

        Pricing pricingMEX = Pricing.Builder.aPricing()
                .currency("MEX")
                .price(new BigDecimal(30))
                .build();

        List<Pricing> pricingList = new ArrayList<>();
        pricingList.add(pricingEUR);
        pricingList.add(pricingUSD);
        pricingList.add(pricingMEX);

        return pricingList;

    }

    public static List<Pricing> getPricings20Discount() {

        Pricing pricingEUR = Pricing.Builder.aPricing()
                .currency("EUR")
                .discountPercentage(new BigDecimal(20))
                .build();

        Pricing pricingUSD = Pricing.Builder.aPricing()
                .currency("USD")
                .discountPercentage(new BigDecimal(20))
                .build();

        Pricing pricingMEX = Pricing.Builder.aPricing()
                .currency("MEX")
                .discountPercentage(new BigDecimal(30))
                .build();

        List<Pricing> pricingList = new ArrayList<>();
        pricingList.add(pricingEUR);
        pricingList.add(pricingUSD);
        pricingList.add(pricingMEX);

        return pricingList;

    }
}
