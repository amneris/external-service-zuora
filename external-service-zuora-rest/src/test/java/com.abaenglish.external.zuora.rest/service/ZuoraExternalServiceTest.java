package com.abaenglish.external.zuora.rest.service;


import com.abaenglish.external.zuora.rest.domain.catalog.Catalog;
import com.abaenglish.external.zuora.rest.domain.hostedpage.HostedList;
import com.abaenglish.external.zuora.rest.domain.signature.ResponseSignature;
import com.abaenglish.external.zuora.rest.domain.signature.Signature;
import com.abaenglish.external.zuora.rest.objectmother.*;
import com.abaenglish.external.zuora.rest.properties.ZuoraRestProperties;
import com.abaenglish.external.zuora.rest.service.impl.ZuoraExternalServiceRest;
import org.junit.*;
import org.mockito.*;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

public class ZuoraExternalServiceTest {

    @InjectMocks
    private ZuoraExternalServiceRest zuoraExternalService;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private ZuoraRestProperties zuoraRestProperties;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void getCatalog() {

        //TODO: Refactor Spring 4.3 http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/test/web/client/MockRestServiceServer.html

        Catalog catalog = CatalogObjectMother.defaultCatalog();
        ResponseEntity<Catalog> catalogResponseEntity = new ResponseEntity<Catalog>(catalog, HttpStatus.OK);

        Mockito.when(zuoraRestProperties.getProxy()).thenReturn("http://zuorasandbox-apitools.abaenglish.com:10002/rest/v1");
        Mockito.when(restTemplate.getForEntity("http://zuorasandbox-apitools.abaenglish.com:10002/rest/v1/catalog/products?pageSize=99", Catalog.class)).thenReturn(catalogResponseEntity);

        Catalog catalog1 = zuoraExternalService.getCatalog();

        assertNotNull(catalog1);
        assertThat(catalog.getProducts().get(0)).isEqualTo(catalog1.getProducts().get(0));
        assertThat(catalog.getSuccess()).isEqualTo(catalog1.getSuccess());

    }

    @Test
    public void getHostedList() {
        HostedList hostedList
                = HostedListObjectMother.getHostedList();

        Mockito.when(zuoraRestProperties.getProxy()).thenReturn("http://zuorasandbox-apitools.abaenglish.com:10002/rest/v1");
        Mockito.when(restTemplate.getForObject("http://zuorasandbox-apitools.abaenglish.com:10002/rest/v1/hostedpages", HostedList.class)).thenReturn(hostedList);

        HostedList testHostedList = zuoraExternalService.getHostedList();
        assertNotNull(testHostedList);
        assertThat(hostedList.getHostedpages().get(0).getPageId()).isEqualTo(testHostedList.getHostedpages().get(0).getPageId());
        assertThat(hostedList.getHostedpages().get(0).getPageName()).isEqualTo(testHostedList.getHostedpages().get(0).getPageName());
        assertThat(hostedList.getHostedpages().get(0).getPageType()).isEqualTo(testHostedList.getHostedpages().get(0).getPageType());
        assertThat(hostedList.getHostedpages().get(0).getPageVersion()).isEqualTo(testHostedList.getHostedpages().get(0).getPageVersion());
        assertThat(hostedList.getHostedpages().get(1).getPageId()).isEqualTo(testHostedList.getHostedpages().get(1).getPageId());
        assertThat(hostedList.getHostedpages().get(1).getPageName()).isEqualTo(testHostedList.getHostedpages().get(1).getPageName());
        assertThat(hostedList.isSuccess()).
                isEqualTo(testHostedList.isSuccess());

    }

    @Test
    public void getSignature() {

        Signature requestSignature = SignatureObjectMother.requestSignature();

        ResponseSignature responseSignature = SignatureObjectMother.responseSignature();

        Mockito.when(zuoraRestProperties.getHost()).thenReturn("https://apisandbox-api.zuora.com/rest/v1");
        Mockito.when(restTemplate.postForObject("https://apisandbox-api.zuora.com/rest/v1/rsa-signatures", requestSignature, ResponseSignature.class)).thenReturn(responseSignature);

        ResponseSignature testSignature = zuoraExternalService.getSignature(requestSignature);

        assertNotNull(testSignature);
        assertThat(testSignature.isSuccess()).isEqualTo(responseSignature.isSuccess());
        assertThat(testSignature.getKey()).isEqualTo(responseSignature.getKey());
        assertThat(testSignature.getSignature()).isEqualTo(responseSignature.getSignature());
        assertThat(testSignature.getTenantId()).isEqualTo(responseSignature.getTenantId());
        assertThat(testSignature.getToken()).isEqualTo(responseSignature.getToken());
    }

}