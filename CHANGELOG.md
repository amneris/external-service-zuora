# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased [1.16]
### Add
- Added EffectiveDate to the response from getAmendmentInfo.
- Added new method to obtain subscriptions in zuora by its original id and status
- Added new method to obtain amend information given a subscription id related
- Added new methods to obtain payment details and its invoice related
### Removed
- Removed smallestSubscriptionPeriod from ZuoraSoapProperties.

## [1.15]
### Change
- Changed the method used to obtain refunds, renewals and cancels taking in consideration the full time.

## [1.13]
### Add
- Add property to set mail preferences to the account creation
### Change
- add DueDate attribute to InvoiceInfo response

## [1.9]
### Add
- Added method for get invoice payments information by payment identifier given ('InvoicePayment' is the relation between payment and invoice)
- Changed method for get invoice items information for obtain subscription identifier related to each item.
- Added cancel account in zuora method

## [1.8]
### Add
- Added cancel subscription in zuora method

## [1.7]
### Change
- Added in request subscription originalID vs Id conditional.

## [1.6]
### Change
- Added in request subscription originalID.

## [1.5]
### Change
- Update timezone to GMT -7

## [1.3]
### Change
- Change BillCycleDay 1 to automatic when account was created.

## [1.2]
### Added
- Added Gateway Name in request subscription in object Account.

## [1.1]
### Change
- Change version to 1.1-SNAPSHOT

### Add
- Payment transactions: add InvoiceInfo
